# ProDirect

_To use lombok just install Lombok plugin for IDEA and enable annotation processing._
To generate antlr util classes run command
```mvn antlr4:antlr4```.


See annotations like: ```@Slf4j, @Data, @AllArgsConstructor, @RequiredArgsConstructor, @Builder , @SneakyThrows e.t.c.```