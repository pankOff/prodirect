package ru.pank.off.prodirect.repository;

import ru.pank.off.prodirect.model.SelectionCriteria;

public interface TrafficRepository {
    String getTraffic(String authToken, SelectionCriteria selectionCriteria) throws Exception;
}
