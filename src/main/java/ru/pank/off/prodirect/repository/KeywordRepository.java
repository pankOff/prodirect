package ru.pank.off.prodirect.repository;

import ru.pank.off.prodirect.model.SelectionCriteria;

public interface KeywordRepository {
    String getKeywords(String authToken, SelectionCriteria selectionCriteria) throws Exception;
}
