package ru.pank.off.prodirect.repository.impl;

import ru.pank.off.prodirect.config.APIRequestConfig;
import ru.pank.off.prodirect.utils.HttpClientUtil;
import ru.pank.off.prodirect.utils.RequestBuilder;
import ru.pank.off.prodirect.model.SelectionCriteria;
import ru.pank.off.prodirect.repository.TrafficRepository;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Repository;

@Repository
public class TrafficRepositoryImpl implements TrafficRepository {
   @Override
    public String getTraffic(String authToken, SelectionCriteria selectionCriteria) throws Exception {
        return HttpClientUtil.sendRequest(
                APIRequestConfig.TRAFFIC_URL,
                HttpMethod.POST,
                RequestBuilder.getHeaders(authToken),
                RequestBuilder.getBody(APIRequestConfig.TRAFFIC_GET,selectionCriteria));
    }
}
