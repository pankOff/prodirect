package ru.pank.off.prodirect.repository.impl;

import ru.pank.off.prodirect.config.APIRequestConfig;
import ru.pank.off.prodirect.utils.HttpClientUtil;
import ru.pank.off.prodirect.utils.RequestBuilder;
import ru.pank.off.prodirect.model.SelectionCriteria;
import ru.pank.off.prodirect.repository.BidRepository;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Repository;

@Repository
public class BidRepositoryImpl implements BidRepository {
    @Override
    public String getBids(String authToken, SelectionCriteria selectionCriteria) throws Exception {
        return HttpClientUtil.sendRequest(
                APIRequestConfig.BID_URL,
                HttpMethod.POST,
                RequestBuilder.getHeaders(authToken),
                RequestBuilder.getBody(APIRequestConfig.BIDS_GET,selectionCriteria));
    }

    @Override
    public String setBid(String authToken, SelectionCriteria selectionCriteria) throws Exception {
        return HttpClientUtil.sendRequest(
                APIRequestConfig.BID_URL,
                HttpMethod.POST,
                RequestBuilder.getHeaders(authToken),
                RequestBuilder.getBody(APIRequestConfig.BIDS_SET,selectionCriteria));
    }
}
