package ru.pank.off.prodirect.repository;

import ru.pank.off.prodirect.model.SelectionCriteria;

public interface AdGroupRepository {

    String getAll(String authToken, SelectionCriteria selectionCriteria) throws Exception;
}
