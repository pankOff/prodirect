package ru.pank.off.prodirect.repository.impl;

import ru.pank.off.prodirect.config.APIRequestConfig;
import ru.pank.off.prodirect.utils.HttpClientUtil;
import ru.pank.off.prodirect.utils.RequestBuilder;
import ru.pank.off.prodirect.model.SelectionCriteria;
import ru.pank.off.prodirect.repository.KeywordRepository;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Repository;

@Repository
public class KeywordRepositoryImpl implements KeywordRepository {

    @Override
    public String getKeywords(String authToken, SelectionCriteria selectionCriteria) throws Exception {
        return HttpClientUtil.sendRequest(
                APIRequestConfig.KEYWORD_URL,
                HttpMethod.POST,
                RequestBuilder.getHeaders(authToken),
                RequestBuilder.getBody(APIRequestConfig.KEYWORDS_GET,selectionCriteria));
    }
}
