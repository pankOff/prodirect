package ru.pank.off.prodirect.repository.impl;

import org.springframework.http.HttpMethod;
import ru.pank.off.prodirect.config.APIRequestConfig;
import ru.pank.off.prodirect.model.SelectionCriteria;
import ru.pank.off.prodirect.repository.AdGroupRepository;
import org.springframework.stereotype.Repository;
import ru.pank.off.prodirect.utils.HttpClientUtil;
import ru.pank.off.prodirect.utils.RequestBuilder;

@Repository
public class AdGroupRepositoryImpl implements AdGroupRepository {
    @Override
    public String getAll(String authToken, SelectionCriteria selectionCriteria) throws Exception {
        return HttpClientUtil.sendRequest(
                APIRequestConfig.ADGROUP_URL,
                HttpMethod.POST,
                RequestBuilder.getHeaders(authToken),
                RequestBuilder.getBody(APIRequestConfig.ADGROUP_GET, selectionCriteria));
    }
}
