package ru.pank.off.prodirect.repository;

import ru.pank.off.prodirect.model.SelectionCriteria;
import thirdparty.contextguide.yandexservices.exceptions.ApiRequestException;
import thirdparty.contextguide.yandexservices.exceptions.ApiResponseException;

import java.io.IOException;
import java.util.List;

public interface AdRepository {

    String getAds(String authToken, SelectionCriteria selectionCriteria) throws Exception;
}
