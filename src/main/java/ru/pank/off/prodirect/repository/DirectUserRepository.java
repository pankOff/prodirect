package ru.pank.off.prodirect.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.pank.off.prodirect.model.DirectUser;

import javax.tools.Diagnostic;
import java.util.Optional;

@Repository
public interface DirectUserRepository extends JpaRepository<DirectUser, Long> {

    DirectUser findByUsername(String username);
    DirectUser findByEmail(String email);
    Optional<DirectUser> findById(Long id);
    DirectUser findByDirectAuthToken(String directAuthToken);

}
