package ru.pank.off.prodirect.repository;

import ru.pank.off.prodirect.model.SelectionCriteria;

public interface BidRepository {
    String getBids(String authToken, SelectionCriteria selectionCriteria) throws Exception;
    String setBid(String authToken, SelectionCriteria selectionCriteria) throws Exception;
}
