package ru.pank.off.prodirect.repository.impl;

import org.springframework.http.HttpMethod;
import ru.pank.off.prodirect.config.APIRequestConfig;
import ru.pank.off.prodirect.repository.CampaignRepository;
import org.springframework.stereotype.Repository;
import ru.pank.off.prodirect.utils.HttpClientUtil;
import ru.pank.off.prodirect.utils.RequestBuilder;

@Repository
public class CampaignRepositoryImpl implements CampaignRepository {
    @Override
    public String getAllCampaigns(String authToken) throws Exception {
        return HttpClientUtil.sendRequest(
                APIRequestConfig.CAMPAIGN_URL,
                HttpMethod.POST,
                RequestBuilder.getHeaders(authToken),
                RequestBuilder.getBody(APIRequestConfig.CAMPAIGN_GET));
    }
}
