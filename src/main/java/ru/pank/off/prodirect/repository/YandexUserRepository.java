package ru.pank.off.prodirect.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.pank.off.prodirect.model.YandexUser;

@Repository
public interface YandexUserRepository extends JpaRepository<YandexUser, Long> {
    YandexUser findByUsername(String username);
    YandexUser findByEmail(String email);
    YandexUser findByYandexAuthToken(String yandexAuthToken);
}
