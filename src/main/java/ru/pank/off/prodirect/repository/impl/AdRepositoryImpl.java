package ru.pank.off.prodirect.repository.impl;

import org.springframework.http.HttpMethod;
import ru.pank.off.prodirect.config.APIRequestConfig;
import ru.pank.off.prodirect.model.SelectionCriteria;
import ru.pank.off.prodirect.repository.AdRepository;
import org.springframework.stereotype.Repository;
import ru.pank.off.prodirect.utils.HttpClientUtil;
import ru.pank.off.prodirect.utils.RequestBuilder;
import thirdparty.contextguide.ad.AdFieldEnum;
import thirdparty.contextguide.yandexservices.ads.AdsMethod;
import thirdparty.contextguide.yandexservices.ads.AdsSelectionCriteria;
import thirdparty.contextguide.yandexservices.ads.GetRequest;
import thirdparty.contextguide.yandexservices.exceptions.*;
import thirdparty.contextguide.yandexservices.utils.ServiceConnectionManager;
import thirdparty.contextguide.yandexservices.utils.ServiceConnectionManagerDefaultImpl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Repository
public class AdRepositoryImpl implements AdRepository {
    @Override
    public String getAds(String authToken, SelectionCriteria selectionCriteria) throws Exception {
        /*String url = APIRequestConfig.AD_URL;;
        AdsSelectionCriteria criteria = new AdsSelectionCriteria(null, selectionCriteria.getAdGroupIds(),null);
        List<AdFieldEnum> fieldNames = new ArrayList<>();
        fieldNames.add(AdFieldEnum.Id);
        fieldNames.add(AdFieldEnum.Status);
        GetRequest getRequestAds = new GetRequest(criteria, fieldNames);
        ServiceConnectionManager serviceConnectionManager = new ServiceConnectionManagerDefaultImpl(authToken);
        String response = serviceConnectionManager.sendRequest(AdsMethod.GET,url,getRequestAds);
        return response;*/

        return HttpClientUtil.sendRequest(
                APIRequestConfig.AD_URL,
                HttpMethod.POST,
                RequestBuilder.getHeaders(authToken),
                RequestBuilder.getBody(APIRequestConfig.ADS_GET,selectionCriteria));

    }
}
