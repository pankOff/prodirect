package ru.pank.off.prodirect.dblistening;

import org.postgresql.PGConnection;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ChannelsPoller {

    final
    ListenersRegistry registry;

    private List<Thread> pollingThreads = new ArrayList<>();

    private final
    Map<PgNotificationHelper, Connection> notificationHelpers;

    public ChannelsPoller(ListenersRegistry registry) {
        this.notificationHelpers = registry.getAllRegisteredHelpers();
        this.registry = registry;
    }

    public void startPolling() {
        for (Map.Entry<PgNotificationHelper, Connection> entry : notificationHelpers.entrySet()) {
            Thread thread = new Thread(new PollingRunnable(entry.getValue(), entry.getKey()));
            thread.setDaemon(true);//useful?
            thread.start();
        }
    }

    public void stopPolling() {
        for (Thread thread : pollingThreads)
            thread.interrupt();
    }

    class PollingRunnable implements Runnable {

        private final Connection conn;
        private final PgNotificationHelper helper;

        PollingRunnable(Connection conn, PgNotificationHelper helper) {
            this.conn = conn;
            this.helper = helper;
        }

        @Override
        public void run() {
            int pid = 0;
            PGConnection pgConn;
            try {
                if (conn.isWrapperFor(PGConnection.class)) {
                    pgConn = conn.unwrap(PGConnection.class);
                    pid = pgConn.getBackendPID();
                    while (true) {
                        helper.poll((Connection) pgConn, pid, true);
                        Thread.sleep(500);
                        if (Thread.currentThread().isInterrupted())
                            throw new InterruptedException();
                    }
                } else {
                    System.out.println("WARN: The connections from pool is not wrappers of PGConnection" +
                    " To handle DB poLling another functionality should be implemented");
                }
            } catch (SQLException | InterruptedException e) {
                e.printStackTrace();//TODO: handle exeption somehow
            } finally {
                try {
                    conn.close();
                    notificationHelpers.remove(helper);
                    for (String s : helper.getListenedNames()) {
                        registry.unregister(s);
                    }
                } catch (SQLException e) {
                    e.printStackTrace();//TODO: handle exeption somehow
                }
            }
        }
    }

}
