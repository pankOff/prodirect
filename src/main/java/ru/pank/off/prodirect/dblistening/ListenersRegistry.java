package ru.pank.off.prodirect.dblistening;

import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//TODO: make ListenersRegistry singletone bean.

@Component
public class ListenersRegistry {

    private final HikariDataSource dataSource;

    private final
    List<PgNotificationListener> listeners = new ArrayList<>();

    private final
    Map<PgNotificationHelper, Connection> notificationHelpers = new HashMap<>();

    @Autowired
    public ListenersRegistry(@Qualifier("dataSource") HikariDataSource dataSource) {
        this.dataSource = dataSource;
        listeners.add(new KeywordTableUpdateListener());
    }

    public void registerAll() throws SQLException {
        for (PgNotificationListener listener : listeners) {
            register(listener);
        }
    }

    public void register(String key) throws SQLException {
        for (PgNotificationListener listener : listeners) {
            if(listener.getChannelName().equals(key)){
                register(listener);
            }
        }
    }

    public void unregister(String key) throws SQLException {
        for (PgNotificationListener listener : listeners) {
            if (listener.getChannelName().equals(key)) {
                for (Map.Entry<PgNotificationHelper, Connection> entry : notificationHelpers.entrySet()) {
                    if (entry.getKey().getListenedNames().contains(key)) { //we have only one name per helper
                        entry.getValue().close();
                        notificationHelpers.remove(entry);
                    }
                }
            }
        }
    }

    private void register(PgNotificationListener listener) throws SQLException {
        Connection conn = dataSource.getConnection();
        PgNotificationHelper helper = new PgNotificationHelper();
        helper.addNotificationListener(listener);
        helper.listen(conn, listener.getChannelName());
        notificationHelpers.put(helper, conn);
    }

    public Map<PgNotificationHelper, Connection> getAllRegisteredHelpers() {
        return notificationHelpers;
    }
}
