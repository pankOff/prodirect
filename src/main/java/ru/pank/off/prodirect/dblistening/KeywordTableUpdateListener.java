package ru.pank.off.prodirect.dblistening;

import org.postgresql.PGNotification;

import java.sql.Connection;
import java.util.Collection;
import java.util.List;

public class KeywordTableUpdateListener implements PgNotificationListener {

    private String channel = "keyword_update";

    @Override
    public void notified(PgNotificationHelper helper, long receivingBackendPid, List<PGNotification> notices) {
        System.out.println("NOTIFIED!!!!!!!!!!!");
    }

    @Override
    public void listenersAdded(PgNotificationHelper helper, Connection conn, Collection<String> names) {

    }

    @Override
    public void listenersRemoved(PgNotificationHelper helper, Connection conn, Collection<String> names) {

    }

    @Override
    public String getChannelName() {
        return channel;
    }
}
