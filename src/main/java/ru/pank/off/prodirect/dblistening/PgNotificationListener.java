
package ru.pank.off.prodirect.dblistening;

import org.postgresql.PGNotification;

import java.sql.Connection;
import java.util.Collection;
import java.util.List;

/**
 * Listener interface to be implemented by classes that are interested
 * in hearing about PostgreSQL `NOTIFY' events.
 * <p>
 * Your listeners must never throw, or they will interfere with
 * event dispatch to other listeners.
 */

public interface PgNotificationListener {


/**
     * Reports that a NOTIFY event has been received.
     * <p>
     * Self-notifications will be delivered, so if you wish to exclude them
     * or identify them, test `receivingBackendPid' against
     * `PGNotification.getPid()' for each notification to see if the sending
     * and receiving backend pid are the same.
     *
     * @param helper              Helper that received the NOTIFY
     * @param receivingBackendPid pid of backend that recieved the notification. Test against notices.getPid() to eliminate self notifications.
     * @param notices             Notification events
     */


    void notified(PgNotificationHelper helper, long receivingBackendPid, List<PGNotification> notices);


/**
     * A LISTEN has been executed on the database, and NOTIFY events for that
     * name will now be received.
     * <p>
     * listenersAdded will be called if a new connection has been
     * supplied and has just been listened on. In this case names were
     * already on the list of names to listen to, but a new LISTEN was
     * just issued for each. If the old connection was broken it's possible
     * that notifications were lost during the gap, so the app may need
     * to act on that.
     *
     * @param helper Poller that added the listen
     * @param names  Read-only list of name(s) newly LISTENed
     */
    void listenersAdded(PgNotificationHelper helper, Connection conn, Collection<String> names);

/**
     * An UNLISTEN has been executed on the database, and NOTIFY events for that
     * name will no longer be received.
     * <p>
     * '*' will never be passed as a name. Instead, individual
     * listenRemoved(...) calls will be made.
     * <p>
     * listenersRemoved will <b>not</b> be called if the connection has been
     * lost. This means that listenersRemoved isn't perfectly symmetric with
     * listenersAdded. If the connection is dropped, listenersRemoved will
     * not be called but listenersAdded will be.
     *
     * @param helper Poller that added the listen
     * @param names  Read-only list of name(s) newly UNLISTENed
     */

    void listenersRemoved(PgNotificationHelper helper, Connection conn, Collection<String> names);

    String getChannelName();
}

