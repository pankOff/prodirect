package ru.pank.off.prodirect.config;

public class APIRequestConfig {

    //Production API

    public static final String CAMPAIGN_URL = "https://api.direct.yandex.com/json/v5/campaigns";
    public static final String ADGROUP_URL = "https://api.direct.yandex.com/json/v5/adgroups";
    public static final String AD_URL ="https://api.direct.yandex.com/json/v5/ads";
    public static final String KEYWORD_URL = "https://api.direct.yandex.com/json/v5/keywords";
    public static final String BID_URL = "https://api.direct.yandex.com/json/v5/bids";
    public static final String TRAFFIC_URL = "https://api.direct.yandex.com/json/v5/keywordbids";

    //public static final String AUTH_TOKEN = "AQAAAAAGyASIAAVUwVWLLv55KE7jgmtp69SbO_0"; - ivan.pankov.365
    //public static final String AUTH_TOKEN = "AQAAAAAZxtUsAAVUwThTtp-9fUk7gQ9W3MNu-gI";


    //SandBox API
   /* public static final String CAMPAIGN_URL = "https://api-sandbox.direct.yandex.com/json/v5/campaigns";
    public static final String ADGROUP_URL = "https://api-sandbox.direct.yandex.com/json/v5/adgroups";
    public static final String AD_URL ="https://api-sandbox.direct.yandex.com/json/v5/ads";
    public static final String KEYWORD_URL = "https://api-sandbox.direct.yandex.com/json/v5/keywords";
    public static final String BID_URL = "https://api-sandbox.direct.yandex.com/json/v5/bids";
    public static final String TRAFFIC_URL = "https://api-sandbox.direct.yandex.com/json/v5/keywordbids";*/

    //public static final String AUTH_TOKEN = "AgAAAAAGyASIAAVUBNQ2hZqmGkMsoa31wszk64A";  //test token for ivan.pankov.365@yandex.ru
    //public static final String AUTH_TOKEN = "AgAAAAA_tvNPAAZOf2UQfQr6-U6noqvNcBWYM90"; //test token for ivan.pank.off@yandex.ru

    //entity config
    public static final String CAMPAIGN_GET = "CAMPAIGN_GET";
    public static final String ADGROUP_GET = "ADGROUP_GET";
    public static final String KEYWORDS_GET = "KEYWORDS_GET";
    public static final String ADS_GET = "ADS_GET";
    public static final String TRAFFIC_GET = "TRAFFIC_GET";
    public static final String BIDS_GET = "BIDS_GET";
    public static final String BIDS_SET = "BIDS_SET";
}
