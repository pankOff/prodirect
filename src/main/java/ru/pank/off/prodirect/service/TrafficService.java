package ru.pank.off.prodirect.service;

import ru.pank.off.prodirect.model.SelectionCriteria;

public interface TrafficService {
    String getTraffic(String authToken, SelectionCriteria selectionCriteria) throws Exception;
}
