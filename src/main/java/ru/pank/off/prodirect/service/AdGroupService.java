package ru.pank.off.prodirect.service;

import java.util.List;

public interface AdGroupService {

    String getAllAddGroups(String authToken, List<Long> campaignsIds) throws Exception;
}
