package ru.pank.off.prodirect.service;

import ru.pank.off.prodirect.model.SelectionCriteria;

public interface KeywordService {
    String getKeywords(String authToken, SelectionCriteria selectionCriteria) throws Exception;
}
