package ru.pank.off.prodirect.service.impl;

import org.json.JSONArray;
import org.json.JSONObject;
import ru.pank.off.prodirect.model.DirectUser;
import ru.pank.off.prodirect.model.YandexUser;
import ru.pank.off.prodirect.repository.CampaignRepository;
import ru.pank.off.prodirect.service.CampaignService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.pank.off.prodirect.service.DirectUserService;
import java.util.Set;

@Service
public class CampaignServiceImpl implements CampaignService {
    @Autowired
    private CampaignRepository campaignRepository;
    @Autowired
    private DirectUserService directUserService;
    @Autowired
    private CampaignService campaignService;

    @Override
    public String getAllCampaignsForDirectUser(String directAuthToken) throws Exception {
        DirectUser directUser = directUserService.auth(directAuthToken);

        if(directUser == null) {
            return null;
        }

        JSONObject result = new JSONObject();
        JSONArray campaigns = new JSONArray();
        Set<YandexUser> yandexUsers = directUser.getYandexUsers();
        for(YandexUser yandexUser : yandexUsers) {
            String stringResponse = campaignService.getAllCampaigns(yandexUser.getYandexAuthToken());
            JSONObject jsonResponse = new JSONObject(stringResponse);
            jsonResponse = (JSONObject) jsonResponse.get("result");
            JSONArray campaignsArray = (JSONArray) jsonResponse.get("Campaigns");
            for (int i = 0; i < campaignsArray.length(); i++) {
                campaigns.put((campaignsArray.get(i)));
            }
        }
        JSONObject campaignsJson = new JSONObject();
        campaignsJson.put("Campaigns", campaigns);
        result.put("result", campaignsJson);
        result.put("Status", true);
        return result.toString();
    }

    @Override
    public String getAllCampaigns(String authToken) throws Exception {
        return campaignRepository.getAllCampaigns(authToken);
    }

    @Override
    public String getAllCampaignsAndYandexUsersForDirectUser(String directAuthToken) throws Exception {
        DirectUser directUser = directUserService.auth(directAuthToken);
        Set<YandexUser> yandexUsers = directUser.getYandexUsers();
        JSONObject result = new JSONObject();
        JSONArray yandexUsersJson = new JSONArray();
        for(YandexUser yandexUser : yandexUsers) {
            String stringResponse = campaignService.getAllCampaigns(yandexUser.getYandexAuthToken());
            JSONObject jsonResponse = new JSONObject(stringResponse);
            jsonResponse = (JSONObject) jsonResponse.get("result");
            JSONArray campaignsArray = (JSONArray) jsonResponse.get("Campaigns");
            JSONObject yandexUserJson = new JSONObject();
            yandexUserJson.put("username", yandexUser.getUsername());
            yandexUserJson.put("email", yandexUser.getEmail());
            yandexUserJson.put("campaigns", campaignsArray);
            yandexUsersJson.put(yandexUserJson);
        }
        result.put("users", yandexUsersJson);
        result.put("Status", true);
        return result.toString();
    }

    private String getCampaignsForYandexUser(YandexUser yandexUser) throws Exception {
        String stringResponse = campaignService.getAllCampaigns(yandexUser.getYandexAuthToken());
        JSONObject jsonResponse = new JSONObject(stringResponse);
        jsonResponse = (JSONObject) jsonResponse.get("result");
        JSONArray campaignsArray = (JSONArray) jsonResponse.get("Campaigns");

        return null;
    }
}
