package ru.pank.off.prodirect.service.impl;

import ru.pank.off.prodirect.model.SelectionCriteria;
import ru.pank.off.prodirect.repository.AdRepository;
import ru.pank.off.prodirect.service.AdService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import thirdparty.contextguide.yandexservices.exceptions.ApiRequestException;
import thirdparty.contextguide.yandexservices.exceptions.ApiResponseException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Service
public class AdServiceImpl implements AdService {
    @Autowired
    private AdRepository adRepository;
    @Override
    public String getAds(String authToken, SelectionCriteria selectionCriteria) throws Exception {
        return adRepository.getAds(authToken, selectionCriteria);
    }
}
