package ru.pank.off.prodirect.service;

import lombok.extern.slf4j.Slf4j;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.pank.off.prodirect.model.DirectUser;
import ru.pank.off.prodirect.model.SelectedEntity;
import ru.pank.off.prodirect.model.YandexUser;
import ru.pank.off.prodirect.repository.YandexUserRepository;
import ru.pank.off.prodirect.strategy.model.AdGroupDirect;
import ru.pank.off.prodirect.strategy.model.CampaignDirect;
import ru.pank.off.prodirect.strategy.model.KeywordDirect;
import ru.pank.off.prodirect.strategy.model.Strategy;
import ru.pank.off.prodirect.strategy.repository.AdGroupDirectRepository;
import ru.pank.off.prodirect.strategy.repository.CampaignDirectRepository;
import ru.pank.off.prodirect.strategy.repository.KeywordDirectRepository;
import ru.pank.off.prodirect.strategy.repository.StrategyRepository;

import java.util.List;

@Slf4j
@Service
public class StrategyService {

    @Autowired
    private CampaignDirectRepository campaignDirectRepository;
    @Autowired
    private AdGroupDirectRepository adGroupDirectRepository;
    @Autowired
    private KeywordDirectRepository keywordDirectRepository;
    @Autowired
    private DirectUserService directUserService;
    @Autowired
    private YandexUserRepository yandexUserRepository;
    @Autowired
    private StrategyRepository strategyRepository;

    public String setEntitiesForStrategy(String directAuthToken, SelectedEntity selectedEntity) {
        DirectUser directUser = directUserService.auth(directAuthToken);

        System.out.println("SET");
        log.debug("setEntitiesForStrategy, directAuthToken {}, selectedEntity {}",
                directAuthToken, selectedEntity);
        List<Long> campaignIds = selectedEntity.getCampaigns();
        List<Long> adGroupIds = selectedEntity.getAdGroups();
        List<Long> keywordIds = selectedEntity.getKeywords();

        YandexUser yandexUser = yandexUserRepository.findByEmail(selectedEntity.getYandexUser().getEmail());
        Strategy mainStrategy = strategyRepository.findByName(selectedEntity.getMainStrategy().getName());
        Strategy secondStrategy = strategyRepository.findByName(selectedEntity.getSecondStrategy().getName());
        Long mainStrategyMaxBid = selectedEntity.getMainStrategyMaxBid();
        Long secondStrategyMaxBid = selectedEntity.getSecondStrategyMaxBid();

        for (Long id : campaignIds) {
            CampaignDirect campaignDirectDb = campaignDirectRepository.findByExternalId(id);
            if (campaignDirectDb != null) {
                campaignDirectRepository.delete(campaignDirectDb);
            }

            CampaignDirect campaignDirect = new CampaignDirect();
            campaignDirect.setExternalId(id);
            campaignDirect.setMainStrategy(mainStrategy);
            campaignDirect.setSecondStrategy(secondStrategy);
            campaignDirect.setYandexUser(yandexUser);
            campaignDirect.setMainStrategyMaxBid(mainStrategyMaxBid);
            campaignDirect.setSecondStrategyMaxBid(secondStrategyMaxBid);

            campaignDirectRepository.saveAndFlush(campaignDirect);
        }

        for (Long id : adGroupIds) {
            AdGroupDirect adGroupDirectDb = adGroupDirectRepository.findByExternalId(id);
            if (adGroupDirectDb != null) {
                adGroupDirectRepository.delete(adGroupDirectDb);
            }

            AdGroupDirect adGroupDirect = new AdGroupDirect();
            adGroupDirect.setExternalId(id);
            adGroupDirect.setMainStrategy(mainStrategy);
            adGroupDirect.setSecondStrategy(secondStrategy);
            adGroupDirect.setYandexUser(yandexUser);
            adGroupDirect.setMainStrategyMaxBid(mainStrategyMaxBid);
            adGroupDirect.setSecondStrategyMaxBid(secondStrategyMaxBid);

            adGroupDirectRepository.saveAndFlush(adGroupDirect);
        }

        for (Long id : keywordIds) {
            KeywordDirect keywordDirectDb = keywordDirectRepository.findByExternalId(id);
            if (keywordDirectDb != null) {
                keywordDirectRepository.delete(keywordDirectDb);
            }

            KeywordDirect keywordDirect = new KeywordDirect();
            keywordDirect.setExternalId(id);
            keywordDirect.setMainStrategy(mainStrategy);
            keywordDirect.setSecondStrategy(secondStrategy);
            keywordDirect.setYandexUser(yandexUser);
            keywordDirect.setMainStrategyMaxBid(mainStrategyMaxBid);
            keywordDirect.setSecondStrategyMaxBid(secondStrategyMaxBid);
            keywordDirectRepository.saveAndFlush(keywordDirect);
        }

        JSONObject response = new JSONObject();
        response.put("Status", true);
        return response.toString();
    }


    public String getStrategies() {

        JSONArray strategies = new JSONArray();
        System.out.println(strategyRepository.findAll());
        for (Strategy strategy : strategyRepository.findAll()) {
            JSONObject strategyJson = new JSONObject();
            strategyJson.put("id", strategy.getId());
            strategyJson.put("name", strategy.getName());
            strategyJson.put("description", strategy.getDescription());
            strategies.put(strategyJson);
        }
        JSONObject response = new JSONObject();
        response.put("strategies", strategies);
        response.put("Status", true);
        return response.toString();
    }

    public void create(Strategy strategy, Long directUserId) {
        log.info("Create new strategy {} for user with id {}", strategy.getName(), directUserId);
        //TODO validate using ANTLR
        strategy.setDirectUserId(directUserId);
        strategyRepository.save(strategy);
    }

    public void update(Strategy strategy, Long directUserId) {
        log.info("Update strategy {} for user with id {}", strategy.getName(), directUserId);
        //TODO validate using ANTLR
        strategy.setDirectUserId(directUserId);
        strategyRepository.save(strategy);
    }

    public void delete(Long id) {
        log.info("Delete strategy with id {}", id);
        strategyRepository.deleteById(id);
    }

    public Strategy findByName(String strategyName) {
        Strategy strategy = strategyRepository.findByName(strategyName);
        if (strategy == null) {
            log.error("Can't find strategy with name {}", strategyName);
            throw new RuntimeException("Can't find strategy with name " + strategyName);
        }
        return strategy;
    }

}
