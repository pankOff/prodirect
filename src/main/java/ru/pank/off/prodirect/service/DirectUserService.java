package ru.pank.off.prodirect.service;

import ru.pank.off.prodirect.model.DirectUser;
import ru.pank.off.prodirect.model.YandexUser;

import java.util.List;
import java.util.Optional;

public interface DirectUserService {
    List<DirectUser> getAll();
    Optional<DirectUser> getById(Long id);
    DirectUser getByUsername(String username);
    DirectUser getByEmail(String email);
    String addDirectUser(DirectUser user);
    boolean addNewYandexUser(YandexUser user);

    DirectUser getDirectUserFromDb(DirectUser directUserValue);
    String login(DirectUser directUserValue);

    String getYandexUsers(String directAuthToken);

    DirectUser auth(String directAuthToken);
    String getAuthTokenForYandexUserEmail(DirectUser directUser, String yandexUserEmail);
}
