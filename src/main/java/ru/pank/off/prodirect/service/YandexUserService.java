package ru.pank.off.prodirect.service;

import ru.pank.off.prodirect.model.YandexUser;
import java.util.Optional;

public interface YandexUserService {
    Optional<YandexUser> getById(Long id);
    YandexUser getByUsername(String username);
    YandexUser getByEmail(String email);
    String addYandexUser(String directAuthToken, YandexUser userValue);

}
