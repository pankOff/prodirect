package ru.pank.off.prodirect.service.impl;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.pank.off.prodirect.model.DirectUser;
import ru.pank.off.prodirect.model.YandexUser;
import ru.pank.off.prodirect.repository.DirectUserRepository;
import ru.pank.off.prodirect.repository.YandexUserRepository;
import ru.pank.off.prodirect.service.DirectUserService;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

@Service
public class DirectUserServiceImpl implements DirectUserService {

    private final DirectUserRepository repository;
    private final YandexUserRepository yandexUserRepository;

    @Autowired
    public DirectUserServiceImpl(DirectUserRepository repository, YandexUserRepository yandexUserRepository) {
        this.repository = repository;
        this.yandexUserRepository = yandexUserRepository;
    }
    @Override
    public List<DirectUser> getAll() {
        return repository.findAll();
    }

    @Override
    public Optional<DirectUser> getById(Long id) {
        return repository.findById(id);
    }

    @Override
    public DirectUser getByUsername(String username) {
        return repository.findByUsername(username);
    }

    @Override
    public DirectUser getByEmail(String email){
        return repository.findByEmail(email);
    }

    @Override
    public String addDirectUser(DirectUser directUserValue) {
        JSONObject checkForExisting = getDirectUserFromDbByEmailOrUsername(directUserValue);
        JSONObject response = new JSONObject();

        if (!(boolean) checkForExisting.get("isExists")) {
            directUserValue.setActive(true);
            String authToken = UUID.randomUUID().toString();
            directUserValue.setDirectAuthToken(authToken);
            repository.saveAndFlush(directUserValue);
            response.put("directAuthToken", directUserValue.getDirectAuthToken());
            response.put("Status", true);
            return response.toString();
        } else {
            if (checkForExisting.has("errorMessageEmail")) {
                response.put("errorMessageEmail", checkForExisting.get("errorMessageEmail"));
            }
            if (checkForExisting.has("errorMessageUsername")) {
                response.put("errorMessageUsername", checkForExisting.get("errorMessageUsername"));
            }
            response.put("Status", false);
        }
        return response.toString();
    }

    private JSONObject getDirectUserFromDbByEmailOrUsername(DirectUser directUserValue) {
        DirectUser userFromDbByEmail = getByEmail(directUserValue.getEmail());
        DirectUser userFromDbByUsername = getByUsername(directUserValue.getUsername());
        JSONObject response =  new JSONObject();
        if (userFromDbByEmail == null && userFromDbByUsername == null) {
            response.put("isExists", false);
            return response;
        }

        if (userFromDbByEmail != null) {
            response.put("errorMessageEmail","Пользователь с таким email уже существует");
            response.put("isExists", true);
        }

        if (userFromDbByUsername != null) {
            response.put("errorMessageUsername","Пользователь с таким логином уже существует");
            response.put("isExists", true);
        }
        return response;
    }

    @Override
    public boolean addNewYandexUser(YandexUser yandexUser) {
        DirectUser directUserFromDb = getById(yandexUser.getManager().getId()).orElse(null);
        if (directUserFromDb == null) {
            return false;
        }
        Set<YandexUser> yandexUsers = directUserFromDb.getYandexUsers();
        yandexUsers.add(yandexUser);
        directUserFromDb.setYandexUsers(yandexUsers);
        return true;
    }

    @Override
    public String login(DirectUser directUserValue) {
        JSONObject checkForExisting = getDirectUserFromDbByEmailOrUsername(directUserValue);
        JSONObject response = new JSONObject();
        if ((boolean) checkForExisting.get("isExists")) {
            DirectUser directUser = getDirectUserFromDb(directUserValue);
            if (directUser != null) {
                response.put("directAuthToken", directUser.getDirectAuthToken());
                response.put("Status", true);
            } else {
                response.put("errorMessagePassword", "Неверный пароль");
                response.put("Status", false);
            }
        } else {
            response.put("errorMessageLogin", "Пользователь с таким логином не существует");
            response.put("Status", false);
        }

        return response.toString();
    }

    public DirectUser getDirectUserFromDb(DirectUser directUserValue) {
        String password = directUserValue.getPassword();

        DirectUser directUserFromDb = repository.findByEmail(directUserValue.getEmail());
        if (directUserFromDb == null) {
            directUserFromDb = repository.findByUsername(directUserValue.getUsername());
            if (directUserFromDb != null) {
                if (directUserFromDb.getPassword().equals(password)) {
                    return directUserFromDb;
                } else return null;
            }
        } else {
            if (directUserFromDb.getPassword().equals(password)) {
                return directUserFromDb;
            } else return null;
        }
        return null;
    }

    @Override
    public String getYandexUsers(String directAuthToken) {
        JSONObject result = new JSONObject();
        DirectUser directUser = auth(directAuthToken);
        if(directUser != null) {
            JSONArray yandexUsers = new JSONArray();
            for (YandexUser yandexUser : directUser.getYandexUsers()) {
                JSONObject yandexUserJson = new JSONObject();
                yandexUserJson.put("username", yandexUser.getUsername());
                yandexUserJson.put("email", yandexUser.getEmail());
                yandexUsers.put(yandexUserJson);
            }
            result.put("Status", true);
            result.put("users", yandexUsers);

        } else {
            result.put("Status", false);
        }

        return result.toString();
    }

    @Override
    public DirectUser auth(String directAuthToken) {
        return repository.findByDirectAuthToken(directAuthToken);
    }

    @Override
    public String getAuthTokenForYandexUserEmail(DirectUser directUser, String yandexUserEmail) {
        String token = "";
        for(YandexUser user : directUser.getYandexUsers()){
            if(user.getEmail().equals(yandexUserEmail)) {
                token = user.getYandexAuthToken();
            }
        }
        return token;
    }

   /* private boolean loginAuthOrPassword(DirectUser directUserFromDb, String password, String authToken) {
        if(directUserFromDb.getDirectAuthToken() != null) {
            if (directUserFromDb.getDirectAuthToken().equals(authToken)) {
                return true;
            } else {
                if (password != null) {
                    if (directUserFromDb.getPassword().equals(password)) {
                        directUserFromDb.setDirectAuthToken(authToken);
                        return true;
                    } else {
                        return false;
                    }
                } else return false;
            }
        } else if (password != null) {
            if (directUserFromDb.getPassword().equals(password)) {
                authToken = UUID.randomUUID().toString();
                directUserFromDb.setDirectAuthToken(authToken);
                repository.save(directUserFromDb);
                return true;
            } else {
                return false;
            }
        }
        return false;
    }*/



}
