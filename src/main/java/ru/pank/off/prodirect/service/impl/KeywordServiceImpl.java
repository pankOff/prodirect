package ru.pank.off.prodirect.service.impl;

import ru.pank.off.prodirect.model.SelectionCriteria;
import ru.pank.off.prodirect.repository.KeywordRepository;
import ru.pank.off.prodirect.service.KeywordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class KeywordServiceImpl implements KeywordService {
    @Autowired
    private KeywordRepository keywordRepository;

    @Override
    public String getKeywords(String authToken, SelectionCriteria selectionCriteria) throws Exception {
        return keywordRepository.getKeywords(authToken, selectionCriteria);
    }
}
