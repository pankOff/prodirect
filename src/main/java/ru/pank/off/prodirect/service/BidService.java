package ru.pank.off.prodirect.service;

import ru.pank.off.prodirect.model.SelectionCriteria;

public interface BidService {
    String getBids(String authToken, SelectionCriteria selectionCriteria) throws Exception;
    String setBid(String authToken, SelectionCriteria selectionCriteria) throws Exception;
}
