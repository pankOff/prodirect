package ru.pank.off.prodirect.service;

public interface CampaignService {
    String getAllCampaignsForDirectUser(String authToken) throws Exception;

    String getAllCampaigns(String authToken) throws Exception;

    String getAllCampaignsAndYandexUsersForDirectUser(String authToken) throws Exception;
}
