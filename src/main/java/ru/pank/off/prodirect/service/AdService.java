package ru.pank.off.prodirect.service;

import ru.pank.off.prodirect.model.SelectionCriteria;
import thirdparty.contextguide.yandexservices.exceptions.ApiRequestException;
import thirdparty.contextguide.yandexservices.exceptions.ApiResponseException;

import java.io.IOException;

public interface AdService {

    String getAds(String authToken, SelectionCriteria selectionCriteria) throws Exception;
}

