package ru.pank.off.prodirect.service.impl;

import ru.pank.off.prodirect.model.SelectionCriteria;
import ru.pank.off.prodirect.repository.TrafficRepository;
import ru.pank.off.prodirect.service.TrafficService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TrafficServiceImpl implements TrafficService {

    @Autowired
    private TrafficRepository trafficRepository;

    @Override
    public String getTraffic(String authToken, SelectionCriteria selectionCriteria) throws Exception {
        return trafficRepository.getTraffic(authToken, selectionCriteria);
    }

}
