package ru.pank.off.prodirect.service.impl;

import ru.pank.off.prodirect.model.SelectionCriteria;
import ru.pank.off.prodirect.repository.BidRepository;
import ru.pank.off.prodirect.service.BidService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BidServiceImpl implements BidService {
    @Autowired
    private BidRepository bidRepository;
    @Override
    public String getBids(String authToken, SelectionCriteria selectionCriteria) throws Exception {
        return bidRepository.getBids(authToken, selectionCriteria);
    }

    @Override
    public String setBid(String authToken, SelectionCriteria selectionCriteria) throws Exception {
        return bidRepository.setBid(authToken, selectionCriteria);
    }
}
