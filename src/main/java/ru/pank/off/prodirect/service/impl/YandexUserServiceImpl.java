package ru.pank.off.prodirect.service.impl;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.pank.off.prodirect.model.DirectUser;
import ru.pank.off.prodirect.model.YandexUser;
import ru.pank.off.prodirect.repository.YandexUserRepository;
import ru.pank.off.prodirect.service.DirectUserService;
import ru.pank.off.prodirect.service.YandexUserService;

import java.util.Optional;

@Service
public class YandexUserServiceImpl implements YandexUserService {

    @Autowired
    private YandexUserRepository yandexUserRepository;

    @Autowired
    private DirectUserService directUserService;

    @Override
    public Optional<YandexUser> getById(Long id) {
        return yandexUserRepository.findById(id);
    }

    @Override
    public YandexUser getByUsername(String username) {
        return yandexUserRepository.findByUsername(username);
    }

    @Override
    public YandexUser getByEmail(String email) {
        return yandexUserRepository.findByEmail(email);
    }

    @Override
    public String addYandexUser(String directAuthToken, YandexUser userValue) {
        DirectUser directUser = directUserService.auth(directAuthToken);
        JSONObject response = new JSONObject();
        if (directUser != null) {
            YandexUser yandexUserFromDb = getByEmail(userValue.getEmail());
            if (yandexUserFromDb == null) {
                yandexUserFromDb = yandexUserRepository.findByYandexAuthToken(userValue.getYandexAuthToken());
                if (yandexUserFromDb == null) {
                    userValue.setManager(directUser);
                    yandexUserRepository.saveAndFlush(userValue);
                    response.put("Status", true);
                } else {
                    response.put("ErrorMessage", "Пользователь уже зарегистрирован");
                    response.put("Status", false);
                }
            } else {
                response.put("ErrorMessage", "Пользователь с таким email уже существует");
                response.put("Status", false);
            }
        } else {
            response.put("ErrorMessage", "Недействительная сессия, перевойдите на портал");
            response.put("Status", false);
        }

        return response.toString();
    }
}
