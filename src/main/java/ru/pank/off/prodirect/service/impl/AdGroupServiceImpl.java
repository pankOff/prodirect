package ru.pank.off.prodirect.service.impl;

import org.json.JSONArray;
import org.json.JSONObject;
import ru.pank.off.prodirect.model.SelectionCriteria;
import ru.pank.off.prodirect.repository.AdGroupRepository;
import ru.pank.off.prodirect.repository.AdRepository;
import ru.pank.off.prodirect.repository.KeywordRepository;
import ru.pank.off.prodirect.service.AdGroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.pank.off.prodirect.service.BidService;
import ru.pank.off.prodirect.strategy.model.KeywordDirect;
import ru.pank.off.prodirect.strategy.service.KeywordDirectService;

import java.util.*;

@Service
public class AdGroupServiceImpl implements AdGroupService {

    @Autowired
    private AdGroupRepository adGroupRepository;
    @Autowired
    private AdRepository adRepository;
    @Autowired
    private KeywordRepository keywordRepository;
    @Autowired
    private BidService bidService;
    @Autowired
    private KeywordDirectService keywordDirectService;

    @Override
    public String getAllAddGroups(String authToken, List<Long> campaignsIds) throws Exception {
        SelectionCriteria selectionCriteria = new SelectionCriteria();
        selectionCriteria.setCampaignsIds(campaignsIds);
        selectionCriteria.setAdGroupIds(Collections.emptyList());
        selectionCriteria.setKeywordIds(Collections.emptyList());
        String adGroupsResponse = adGroupRepository.getAll(authToken, selectionCriteria);
        JSONObject adGroupsJsonResponse = new JSONObject(adGroupsResponse);
        if (adGroupsJsonResponse.has("result")) {
            adGroupsJsonResponse = (JSONObject) adGroupsJsonResponse.get("result");
            JSONArray adGroupsArray = (JSONArray) adGroupsJsonResponse.get("AdGroups");
            List<Long> adGroupsIds = new ArrayList<>();
            for(Object adGroup : adGroupsArray) {
                JSONObject adGroupJson = (JSONObject) adGroup;
                String id = adGroupJson.get("Id").toString();
                adGroupsIds.add(new Long(id));
            }
            selectionCriteria = new SelectionCriteria();
            selectionCriteria.setAdGroupIds(adGroupsIds);
            selectionCriteria.setCampaignsIds(Collections.emptyList());
            String adsResponse = adRepository.getAds(authToken, selectionCriteria);
            String keywordsResponse = keywordRepository.getKeywords(authToken, selectionCriteria);

            JSONObject keywordsJsonResponse = new JSONObject(keywordsResponse);
            JSONObject adsResponseJsonResponse = new JSONObject(adsResponse);

            keywordsJsonResponse = (JSONObject) keywordsJsonResponse.get("result");
            adsResponseJsonResponse = (JSONObject) adsResponseJsonResponse.get("result");

            JSONArray keywords = getKeywordsWithBids(authToken, keywordsJsonResponse);

            JSONObject result = new JSONObject();
            result.put("Keywords", keywords);
            result.put("Ads", adsResponseJsonResponse.get("Ads"));
            result.put("AdGroups", adGroupsJsonResponse.get("AdGroups"));
            result.put("Status", true);

            return result.toString();
        }
        return null;
    }

    private JSONArray getKeywordsWithBids(String authToken, JSONObject keywordsJsonResponse) throws Exception {
        JSONArray keywordsArray = (JSONArray) keywordsJsonResponse.get("Keywords");
        List<Long> keywordsIds = new ArrayList<>();
        Map<Long, String> keywordMap = new HashMap<>();
        for (Object keyword : keywordsArray) {
            JSONObject keywordJson = (JSONObject) keyword;
            Long id = new Long(keywordJson.get("Id").toString());
            String keywordText = keywordJson.get("Keyword").toString();
            keywordMap.put(id, keywordText);
            keywordsIds.add(id);
        }
        SelectionCriteria selectionCriteria = new SelectionCriteria();
        selectionCriteria.setKeywordIds(keywordsIds);
        selectionCriteria.setAdGroupIds(Collections.emptyList());
        selectionCriteria.setCampaignsIds(Collections.emptyList());
        String bidsResponse = bidService.getBids(authToken, selectionCriteria);
        JSONObject bidsJsonResponse = new JSONObject(bidsResponse);
        bidsJsonResponse = (JSONObject) bidsJsonResponse.get("result");
        JSONArray bidsArray = (JSONArray) bidsJsonResponse.get("Bids");

        for (Object bid : bidsArray) {
            JSONObject bidJson = (JSONObject) bid;
            Long keywordId = new Long(bidJson.get("KeywordId").toString());
            bidJson.put("Keyword", keywordMap.get(keywordId));
            KeywordDirect keywordDirect = keywordDirectService.getByYandexId(keywordId);
            if (keywordDirect != null) {
                bidJson.put("Strategy", keywordDirect.getMainStrategy().getDescription());
                bidJson.put("MaxBid", keywordDirect.getMainStrategyMaxBid());
            }
        }

        return bidsArray;
    }
}
