grammar StrategyGrammar;

@header {
    package  ru.pank.off.prodirect.antlr;
}


/*r  : 'hello' ID ;         // match keyword hello followed by an identifier
ID : [a-z]+ ;             // match lower-case identifiers
WS : [ \t\r\n]+ -> skip ; // skip spaces, tabs, newlines*/



/// Parser

prog: expr+ ;

expr : expr '^' expr                  # Pow
     | expr op=('*'|'/') expr         # MulDiv
     | expr op=('+'|'-') expr         # AddSub
     | var=(C1|C2|C3|C4|SC1|SC2|SC3|SC4|M1|M2|M3|M4|MC1|MC2|MC3|MC4|VAL)          # Val
     | '(' expr ')'                   # Parens
     ;

/// Lexer

C1   : '1С';
C2   : '2С';
C3   : '3С';
C4   : '4С';
SC1   : '1СЦ';
SC2   : '2СЦ';
SC3   : '3СЦ';
SC4   : '4СЦ';
M1   : '1М';
M2   : '2М';
M3   : '3М';
M4   : '4М';
MC1   : '1МЦ';
MC2   : '2МЦ';
MC3   : '3МЦ';
MC4   : '4МЦ';
VAL : [0-9]+ ( '.' [0-9]+ )?;
MUL : '*';
DIV : '/';
ADD : '+';
SUB : '-';
POW : '^';
SIN : 'sin';
COS : 'cos';
WS  : (' '|'\r'|'\n') -> channel(HIDDEN);
