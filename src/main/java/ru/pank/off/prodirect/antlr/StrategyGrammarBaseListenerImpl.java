package ru.pank.off.prodirect.antlr;

import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.ErrorNode;
import org.antlr.v4.runtime.tree.TerminalNode;

import java.util.Stack;

import static ru.pank.off.prodirect.antlr.StrategyGrammarParser.VAL;

/**
 * This class provides an empty implementation of {@link StrategyGrammarListener},
 * which can be extended to create a listener which only needs to handle a subset
 * of the available methods.
 */
public class StrategyGrammarBaseListenerImpl implements StrategyGrammarListener {

    private Stack<Double> stack = new Stack<>();
    private boolean resultIsReady = false;
    private VariableMap variableMap;

    public StrategyGrammarBaseListenerImpl(VariableMap variableMap) {
        this.variableMap = variableMap;
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation does nothing.</p>
     */
    @Override
    public void enterProg(StrategyGrammarParser.ProgContext ctx) {
        System.out.println("We are here");
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation does nothing.</p>
     */
    @Override
    public void exitProg(StrategyGrammarParser.ProgContext ctx) {
        System.out.println("exitProg " + ctx.getText());
        resultIsReady = true;
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation does nothing.</p>
     */
    @Override
    public void enterVal(StrategyGrammarParser.ValContext ctx) {
        System.out.println("enterVal " + ctx.getText());
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation does nothing.</p>
     */
    @Override
    public void exitVal(StrategyGrammarParser.ValContext ctx) {
        System.out.println("exitVal " + ctx.getText());
        int token = ctx.var.getType();
        stack.push(token == VAL ? Double.valueOf(ctx.VAL().getText())
                : variableMap.getValue(ctx.var.getType()));
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation does nothing.</p>
     */
    @Override
    public void enterAddSub(StrategyGrammarParser.AddSubContext ctx) {
        System.out.println("enterAddSub " + ctx.getText());
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation does nothing.</p>
     */
    @Override
    public void exitAddSub(StrategyGrammarParser.AddSubContext ctx) {
        System.out.println("exitAddSub" + ctx.getText());
        double operand2 = stack.pop();
        double operand1 = stack.pop();
        stack.push(ctx.op.getType() == StrategyGrammarParser.ADD
                ? operand1 + operand2 : operand1 - operand2);
        System.out.println("RESULT: " + stack.peek());
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation does nothing.</p>
     */
    @Override
    public void enterMulDiv(StrategyGrammarParser.MulDivContext ctx) {
        System.out.println("enterMulDiv " + ctx.getText());
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation does nothing.</p>
     */
    @Override
    public void exitMulDiv(StrategyGrammarParser.MulDivContext ctx) {
        System.out.println("exitMulDiv " + ctx.getText());
        Double operand2 = stack.pop();
        Double operand1 = stack.pop();
        stack.push(ctx.op.getType() == StrategyGrammarParser.MUL
                ? operand1 * operand2 : operand1 / operand2);
        System.out.println("RESULT: " + stack.peek());

    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation does nothing.</p>
     */
    @Override
    public void enterParens(StrategyGrammarParser.ParensContext ctx) {
        System.out.println("enterParens " + ctx.getText());
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation does nothing.</p>
     */
    @Override
    public void exitParens(StrategyGrammarParser.ParensContext ctx) {
        System.out.println("exitParens " + ctx.getText());
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation does nothing.</p>
     */
    @Override
    public void enterPow(StrategyGrammarParser.PowContext ctx) {
        System.out.println("enterPow " + ctx.getText());
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation does nothing.</p>
     */
    @Override
    public void exitPow(StrategyGrammarParser.PowContext ctx) {
        System.out.println("exitPow" + ctx.getText());
        Double operand2 = stack.pop();
        Double operand1 = stack.pop();
        stack.push(Math.pow(operand1, operand2));
        System.out.println("RESULT: " + stack.peek());
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation does nothing.</p>
     */
    @Override
    public void enterEveryRule(ParserRuleContext ctx) {
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation does nothing.</p>
     */
    @Override
    public void exitEveryRule(ParserRuleContext ctx) {
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation does nothing.</p>
     */
    @Override
    public void visitTerminal(TerminalNode node) {
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation does nothing.</p>
     */
    @Override
    public void visitErrorNode(ErrorNode node) {
    }

    public Double getResult() {
        if (resultIsReady) {
            return stack.pop();
        } else {
            throw new RuntimeException("Result not ready yet!");
        }
    }
}