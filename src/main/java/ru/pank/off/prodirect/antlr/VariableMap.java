package ru.pank.off.prodirect.antlr;

import java.util.Map;
import java.util.stream.Collectors;

/**
 * Class to be used by {@link StrategyGrammarBaseListenerImpl}
 * */
public class VariableMap {

    private Map<Integer, Double> embeddedVarsMap;

    public VariableMap(Map<EmbeddedVar, Double> embeddedVariablesMap) {
        embeddedVarsMap = embeddedVariablesMap.entrySet()
                .stream()
                .collect(Collectors.toMap(
                        entry -> entry.getKey().getVarIndex(),
                        Map.Entry::getValue
                ));
    }

    public Double getValue(int varIndex) {
        return embeddedVarsMap.get(varIndex);
    }

    public enum EmbeddedVar {
        C1(StrategyGrammarParser.C1), C2(StrategyGrammarParser.C2), C3(StrategyGrammarParser.C3),
        C4(StrategyGrammarParser.C4), SC1(StrategyGrammarParser.SC1), SC2(StrategyGrammarParser.SC2),
        SC3(StrategyGrammarParser.SC3), SC4(StrategyGrammarParser.SC4), M1(StrategyGrammarParser.M1),
        M2(StrategyGrammarParser.M2), M3(StrategyGrammarParser.M3), M4(StrategyGrammarParser.M4),
        MC1(StrategyGrammarParser.MC1), MC2(StrategyGrammarParser.MC2), MC3(StrategyGrammarParser.MC3),
        MC4(StrategyGrammarParser.MC4);

        private int varIndex;

        EmbeddedVar(int varIndex) {
            this.varIndex = varIndex;
        }

        public int getVarIndex() {
            return varIndex;
        }
    }

    public enum AuctionBidName {
        P11, P12, P13, P14, P21, P22, P23, P24
    }
}
