package ru.pank.off.prodirect.strategy.implement;

import org.json.JSONArray;
import org.json.JSONObject;
import ru.pank.off.prodirect.strategy.service.StrategyBidPreparer;

public class FirstStrategyBidPreparer implements StrategyBidPreparer {
    @Override
    public Long getNewBid(JSONArray auctionBids) {
        Long P11 = new Long(0);

        for(Object auctionBid : auctionBids) {
            JSONObject auctionBidJson = (JSONObject) auctionBid;
            if (auctionBidJson.get("Position").equals("P11")) {
                P11 = new Long(auctionBidJson.get("Bid").toString());
            }
        }
        Long newBid = new Long(0);

        newBid = (new Double(P11+P11*0.1)).longValue();
        return newBid;
    }
}
