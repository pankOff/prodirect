package ru.pank.off.prodirect.strategy.implement;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.json.JSONArray;
import org.json.JSONObject;
import ru.pank.off.prodirect.antlr.StrategyGrammarBaseListenerImpl;
import ru.pank.off.prodirect.antlr.StrategyGrammarLexer;
import ru.pank.off.prodirect.antlr.StrategyGrammarParser;
import ru.pank.off.prodirect.antlr.VariableMap;
import ru.pank.off.prodirect.model.AuctionBid;
import ru.pank.off.prodirect.strategy.model.Strategy;
import ru.pank.off.prodirect.strategy.service.StrategyBidPreparer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Slf4j
public class StoredStrategyPreparer implements StrategyBidPreparer {

    private final Strategy strategy;

    private ObjectMapper objectMapper = new ObjectMapper();

    public StoredStrategyPreparer(Strategy strategy) {
        this.strategy = strategy;
    }

    @Override
    public Long getNewBid(JSONArray auctionBids) {
        List<AuctionBid> auctionBidList = parseBids(auctionBids);
        return evaluate(strategy.getFormula(), prepareVariables(auctionBidList))
                .longValue();
    }

    private Double evaluate(String formula, HashMap<VariableMap.EmbeddedVar, Double> variablesMap) {
        CharStream charStream = CharStreams.fromString(formula);
        StrategyGrammarLexer lexer = new StrategyGrammarLexer(charStream);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        StrategyGrammarParser parser = new StrategyGrammarParser(tokens);

        StrategyGrammarBaseListenerImpl listener =
                new StrategyGrammarBaseListenerImpl(new VariableMap(variablesMap));
        parser.addParseListener(listener);
        ParseTree tree = parser.prog();
        ParseTreeWalker walker = new ParseTreeWalker();
        walker.walk(listener, tree);
        return listener.getResult();
    }

    private HashMap<VariableMap.EmbeddedVar, Double> prepareVariables(
            List<AuctionBid> auctionBids) {
        HashMap<VariableMap.EmbeddedVar, Double> map = new HashMap<>();
        auctionBids.forEach(bid -> {
            switch (bid.getPosition()) {
                case P11:
                    map.put(VariableMap.EmbeddedVar.C1, bid.getBid());
                    map.put(VariableMap.EmbeddedVar.SC1, bid.getPrice());
                case P12:
                    map.put(VariableMap.EmbeddedVar.C2, bid.getBid());
                    map.put(VariableMap.EmbeddedVar.SC2, bid.getPrice());
                case P13:
                    map.put(VariableMap.EmbeddedVar.C3, bid.getBid());
                    map.put(VariableMap.EmbeddedVar.SC3, bid.getPrice());
                case P14:
                    map.put(VariableMap.EmbeddedVar.C4, bid.getBid());
                    map.put(VariableMap.EmbeddedVar.SC4, bid.getPrice());
                case P21:
                    map.put(VariableMap.EmbeddedVar.M1, bid.getBid());
                    map.put(VariableMap.EmbeddedVar.MC1, bid.getPrice());
                case P22:
                    map.put(VariableMap.EmbeddedVar.M2, bid.getBid());
                    map.put(VariableMap.EmbeddedVar.MC2, bid.getPrice());
                case P23:
                    map.put(VariableMap.EmbeddedVar.M3, bid.getBid());
                    map.put(VariableMap.EmbeddedVar.MC3, bid.getPrice());
                case P24:
                    map.put(VariableMap.EmbeddedVar.M4, bid.getBid());
                    map.put(VariableMap.EmbeddedVar.MC4, bid.getPrice());
            }
        });
        return map;

    }

    private List<AuctionBid> parseBids(JSONArray auctionBids) {
        List<AuctionBid> auctionBidList = new ArrayList<>();
        try {
            for (Object auctionBid : auctionBids) {
                JSONObject auctionBidJson = (JSONObject) auctionBid;
                auctionBidList.add(objectMapper.readValue(auctionBidJson.toString(), AuctionBid.class));
            }
        } catch (JsonProcessingException exc) {
            log.error("Failed to parse bids {}", auctionBids);
            throw new RuntimeException(exc);
        }
        return auctionBidList;
    }

}