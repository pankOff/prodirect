package ru.pank.off.prodirect.strategy.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.pank.off.prodirect.model.YandexUser;
import ru.pank.off.prodirect.strategy.model.AdGroupDirect;
import ru.pank.off.prodirect.strategy.model.KeywordDirect;
import ru.pank.off.prodirect.strategy.model.Strategy;
import ru.pank.off.prodirect.strategy.repository.AdGroupDirectRepository;
import ru.pank.off.prodirect.strategy.repository.KeywordDirectRepository;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class AdGroupDirectService {
    @Autowired
    private AdGroupDirectRepository repository;

    public List<Long> getIdsByYandexUserAndMainStrategy(YandexUser yandexUser, Strategy mainStrategy){
        List<Long> idsForStrategy = repository.findByYandexUserAndMainStrategy(yandexUser, mainStrategy)
                .stream().map(AdGroupDirect::getExternalId)
                .collect(Collectors.toList());
        return idsForStrategy;
    }

    public Optional<AdGroupDirect> getById(Long adGroupId) {
        return repository.findById(adGroupId);
    }

    public AdGroupDirect getByYandexId(Long yandexId) {
        return repository.findByExternalId(yandexId);
    }
}
