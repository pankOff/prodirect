package ru.pank.off.prodirect.strategy.service.parser;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class StrategyParser {

    Long parse(String strategy, JSONArray auctionBids) {

        String testStrategy = "((2C*1С+4С)/2.34-3C)-23+2*3";
        return null;
    }

    private Map<String, Long> getBids(JSONArray auctionBids) {

        Map<String, Long> bidsMap = new HashMap<>();
        for(Object auctionBid : auctionBids) {
            JSONObject auctionBidJson = (JSONObject) auctionBid;
            switch (auctionBidJson.get("Position").toString()) {
                case "P11" : {
                    bidsMap.put("1С", new Long(auctionBidJson.get("Bid").toString()));
                    break;
                }
                case "P12" : {
                    bidsMap.put("2С", new Long(auctionBidJson.get("Bid").toString()));
                    break;
                }
                case "P13" : {
                    bidsMap.put("3С", new Long(auctionBidJson.get("Bid").toString()));
                    break;
                }
                case "P14" : {
                    bidsMap.put("4С", new Long(auctionBidJson.get("Bid").toString()));
                    break;
                }

                case "P21" : {
                    bidsMap.put("1М", new Long(auctionBidJson.get("Bid").toString()));
                    break;
                }
                case "P22" : {
                    bidsMap.put("2М", new Long(auctionBidJson.get("Bid").toString()));
                    break;
                }
                case "P23" : {
                    bidsMap.put("3М", new Long(auctionBidJson.get("Bid").toString()));
                    break;
                }
                case "P24" : {
                    bidsMap.put("4М", new Long(auctionBidJson.get("Bid").toString()));
                    break;
                }
                default: break;
            }
        }

        return bidsMap;
    }
}
