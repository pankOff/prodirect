package ru.pank.off.prodirect.strategy.implement;

import org.json.JSONArray;
import org.json.JSONObject;
import ru.pank.off.prodirect.strategy.service.StrategyBidPreparer;

public class OptimalStrategyBidPreparer implements StrategyBidPreparer {
    @Override
    public Long getNewBid(JSONArray auctionBids) {
        Long P11 = new Long(0);
        Long P12 = new Long(0);
        Long P13 = new Long(0);

        for(Object auctionBid : auctionBids) {
            JSONObject auctionBidJson = (JSONObject) auctionBid;
            if (auctionBidJson.get("Position").equals("P11")) {
                P11 = new Long(auctionBidJson.get("Bid").toString());
            }
            if (auctionBidJson.get("Position").equals("P12")) {
                P12 = new Long(auctionBidJson.get("Bid").toString());
            }
            if (auctionBidJson.get("Position").equals("P13")) {
                P13 = new Long(auctionBidJson.get("Bid").toString());
            }
        }
        Long newBid = new Long(0);

        if ((P12-P12*0.05) > P13) {
            newBid = (new Double(P12-P12*0.02)).longValue();
        } else {
            newBid = (new Double(P13+P13*0.01)).longValue();
        }
        return newBid;
    }
}
