package ru.pank.off.prodirect.strategy.service;

import org.json.JSONArray;

public interface StrategyBidPreparer {
    Long getNewBid(JSONArray auctionBids);
}
