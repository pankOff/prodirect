package ru.pank.off.prodirect.strategy.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.pank.off.prodirect.service.StrategyService;
import ru.pank.off.prodirect.strategy.implement.FirstStrategyBidPreparer;
import ru.pank.off.prodirect.strategy.implement.OptimalStrategyBidPreparer;
import ru.pank.off.prodirect.strategy.implement.StoredStrategyPreparer;
import ru.pank.off.prodirect.strategy.implement.StrategyExecutorImpl;

@Service
public class StrategySelector {

    @Autowired
    private StrategyService strategyService;

    public static StrategyExecutor getStrategyExecutor(String strategyName){
        switch (strategyName) {
            case "1stMult1_1": return new StrategyExecutorImpl();
            default: return null;
        }
    }

    public StrategyBidPreparer getStrategyBidPreparer(String strategyName) {
        switch (strategyName) {
            case "1stMult1_1"   : return new FirstStrategyBidPreparer();
            case "optimal"      : return new OptimalStrategyBidPreparer();
            default:
                return new StoredStrategyPreparer(strategyService.findByName(strategyName));
        }
    }
}
