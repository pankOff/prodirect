package ru.pank.off.prodirect.strategy.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.pank.off.prodirect.model.YandexUser;
import ru.pank.off.prodirect.strategy.model.CampaignDirect;
import ru.pank.off.prodirect.strategy.model.Strategy;
import ru.pank.off.prodirect.strategy.repository.CampaignDirectRepository;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class CampaignDirectService {
    @Autowired
    private CampaignDirectRepository repository;

    public List<Long> getIdsByYandexUserAndMainStrategy(YandexUser yandexUser, Strategy mainStrategy){
        List<Long> idsForStrategy = repository.findByYandexUserAndMainStrategy(yandexUser, mainStrategy)
                .stream().map(CampaignDirect::getExternalId)
                .collect(Collectors.toList());
        return idsForStrategy;
    }

    public Optional<CampaignDirect> getById(Long id) {
        return repository.findById(id);
    }

    public CampaignDirect getByYandexId(Long yandexId) {
        return repository.findByExternalId(yandexId);
    }
}
