package ru.pank.off.prodirect.strategy.implement;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.stereotype.Component;
import ru.pank.off.prodirect.model.DirectUser;
import ru.pank.off.prodirect.model.SelectionCriteria;
import ru.pank.off.prodirect.model.YandexUser;
import ru.pank.off.prodirect.service.BidService;
import ru.pank.off.prodirect.service.DirectUserService;
import ru.pank.off.prodirect.strategy.model.AdGroupDirect;
import ru.pank.off.prodirect.strategy.model.CampaignDirect;
import ru.pank.off.prodirect.strategy.model.KeywordDirect;
import ru.pank.off.prodirect.strategy.model.Strategy;
import ru.pank.off.prodirect.strategy.service.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

@Component
public class StrategyExecutorImpl implements StrategyExecutor {

    private DirectUserService directUserService;
    private BidService bidService;
    private KeywordDirectService keywordDirectService;
    private CampaignDirectService campaignDirectService;
    private AdGroupDirectService adGroupDirectService;

    private ConfigurableApplicationContext context;

    @Autowired
    private StrategySelector strategySelector;

    @Override
    public void setContext(ConfigurableApplicationContext context){
        this.context = context;
        directUserService = context.getBean(DirectUserService.class);
        bidService = context.getBean(BidService.class);
        keywordDirectService = context.getBean(KeywordDirectService.class);
        adGroupDirectService = context.getBean(AdGroupDirectService.class);
        campaignDirectService = context.getBean(CampaignDirectService.class);
    }

    private Strategy thisStrategy;

    @Override
    public void execute(Strategy strategy) throws Exception {
        setThisStrategy(strategy);

        List<DirectUser> directUsers = directUserService.getAll();
        for (DirectUser directUser : directUsers) {
            Set<YandexUser> yandexUsers = directUser.getYandexUsers();
            for (YandexUser yandexUser : yandexUsers) {

                // Set Bids for Campaigns
                JSONArray currentBidsForCampaigns = getBidsFromYandex("CAMPAIGN", yandexUser);
                if (currentBidsForCampaigns != null) {
                    setBidsByStrategy("CAMPAIGN", currentBidsForCampaigns, yandexUser.getYandexAuthToken());
                }

                // Set Bids for AdGroups
                JSONArray currentBidsForAdGroups = getBidsFromYandex("AD_GROUP", yandexUser);
                if (currentBidsForCampaigns != null) {
                    setBidsByStrategy("AD_GROUP", currentBidsForAdGroups, yandexUser.getYandexAuthToken());
                }

                // Set Bids for Keywords
                JSONArray currentBidsForKeywords = getBidsFromYandex("KEYWORD", yandexUser);
                if (currentBidsForKeywords != null) {
                    setBidsByStrategy("KEYWORD", currentBidsForKeywords, yandexUser.getYandexAuthToken());
                }


            }
        }

    }

    private JSONArray getBidsFromYandex(String entity, YandexUser yandexUser) throws Exception {
        List<Long> idsForStrategy;
        SelectionCriteria selectionCriteria = new SelectionCriteria();
        selectionCriteria.setKeywordIds(Collections.emptyList());
        selectionCriteria.setAdGroupIds(Collections.emptyList());
        selectionCriteria.setCampaignsIds(Collections.emptyList());
        switch (entity) {
            case "CAMPAIGN" : {
                idsForStrategy = campaignDirectService.getIdsByYandexUserAndMainStrategy(yandexUser, thisStrategy);
                selectionCriteria.setCampaignsIds(idsForStrategy);
                break;
            }

            case "AD_GROUP" : {
                idsForStrategy = adGroupDirectService.getIdsByYandexUserAndMainStrategy(yandexUser, thisStrategy);
                selectionCriteria.setAdGroupIds(idsForStrategy);
                break;
            }

            case "KEYWORD" : {
                idsForStrategy = keywordDirectService.getIdsByYandexUserAndMainStrategy(yandexUser, thisStrategy);
                selectionCriteria.setKeywordIds(idsForStrategy);
                break;
            }

            default: return null;
        }

        if (idsForStrategy.isEmpty()) {
            return null;
        }

        String bidsResponse = bidService.getBids(yandexUser.getYandexAuthToken(), selectionCriteria);
        JSONObject bidsResponseJson = new JSONObject(bidsResponse);

        bidsResponseJson = (JSONObject) bidsResponseJson.get("result");
        return (JSONArray) bidsResponseJson.get("Bids");
    }

    private void setBidsByStrategy(String entity, JSONArray bids, String yandexAuthToken) throws Exception {
        for (Object bid : bids) {
            JSONObject bidJson = (JSONObject) bid;
            JSONArray auctionBids = (JSONArray) bidJson.get("AuctionBids");

            Long mainStrategyMaxBid = null;
            Long secondStrategyMaxBid = null;
            switch (entity) {
                case "CAMPAIGN" : {
                    Long campaignId = new Long(bidJson.get("CampaignId").toString());
                    CampaignDirect campaignDirect = campaignDirectService.getByYandexId(campaignId);
                    if (campaignDirect != null) {
                        mainStrategyMaxBid = campaignDirect.getMainStrategyMaxBid();
                        secondStrategyMaxBid = campaignDirect.getSecondStrategyMaxBid();
                    }
                    break;
                }

                case "AD_GROUP" : {
                    Long adGroupId = new Long(bidJson.get("AdGroupId").toString());
                    AdGroupDirect adGroupDirect = adGroupDirectService.getByYandexId(adGroupId);
                    if (adGroupDirect != null) {
                        mainStrategyMaxBid = adGroupDirect.getMainStrategyMaxBid();
                        secondStrategyMaxBid = adGroupDirect.getSecondStrategyMaxBid();
                    }
                    break;
                }

                case "KEYWORD" : {
                    Long keywordId = new Long(bidJson.get("KeywordId").toString());
                    KeywordDirect keywordDirect = keywordDirectService.getByYandexId(keywordId);
                    if (keywordDirect != null) {
                        mainStrategyMaxBid = keywordDirect.getMainStrategyMaxBid();
                        secondStrategyMaxBid = keywordDirect.getSecondStrategyMaxBid();
                    }
                    break;
                }

                default: {}
            }


            Long currentBid = new Long(bidJson.get("Bid").toString());

            try {
                StrategyBidPreparer strategyBidPreparer = strategySelector.getStrategyBidPreparer(thisStrategy.getName());
                Long newBid = strategyBidPreparer.getNewBid(auctionBids);
                if (mainStrategyMaxBid != null) {
                    if(newBid.equals(currentBid) || (newBid > mainStrategyMaxBid)){
                        continue;
                    }
                }

                SelectionCriteria selectionCriteria = new SelectionCriteria();
                selectionCriteria.setKeywordIds(Collections.emptyList());
                selectionCriteria.setAdGroupIds(Collections.emptyList());
                selectionCriteria.setCampaignsIds(Collections.emptyList());
                switch (entity) {
                    case "CAMPAIGN" : {
                        List<Long> campaignsIds = new ArrayList<>();
                        campaignsIds.add(new Long(bidJson.get("CampaignId").toString()));
                        selectionCriteria.setCampaignsIds(campaignsIds);
                        break;
                    }

                    case "AD_GROUP" : {
                        List<Long> adGroupIds = new ArrayList<>();
                        adGroupIds.add(new Long(bidJson.get("AdGroupId").toString()));
                        selectionCriteria.setAdGroupIds(adGroupIds);
                        break;
                    }

                    case "KEYWORD" : {
                        List<Long> keywordIs = new ArrayList<>();
                        keywordIs.add(new Long(bidJson.get("KeywordId").toString()));
                        selectionCriteria.setKeywordIds(keywordIs);
                        break;
                    }

                    default: {}
                }


                selectionCriteria.setBid(newBid);

                String response = bidService.setBid(yandexAuthToken, selectionCriteria);

            } catch (Exception e) {
                System.out.println(e.getMessage());
            }

        }
    }

    public Strategy getThisStrategy() {
        return thisStrategy;
    }

    public void setThisStrategy(Strategy thisStrategy) {
        this.thisStrategy = thisStrategy;
    }
}
