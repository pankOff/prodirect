package ru.pank.off.prodirect.strategy.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.pank.off.prodirect.strategy.model.Strategy;

import java.util.List;
import java.util.Optional;

@Repository
public interface StrategyRepository extends JpaRepository<Strategy, Long> {
    Strategy findByName(String name);
    Optional<Strategy> findById(Long id);
    List<Strategy> findAll();
}
