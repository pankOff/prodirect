package ru.pank.off.prodirect.strategy.service;


import org.springframework.context.ConfigurableApplicationContext;
import ru.pank.off.prodirect.strategy.model.Strategy;

public interface StrategyExecutor {
    void execute(Strategy strategy) throws Exception;
    void setContext(ConfigurableApplicationContext context);
}
