package ru.pank.off.prodirect.strategy.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.pank.off.prodirect.model.YandexUser;
import ru.pank.off.prodirect.strategy.model.KeywordDirect;
import ru.pank.off.prodirect.strategy.model.Strategy;

import java.util.List;
import java.util.Optional;

@Repository
public interface KeywordDirectRepository extends JpaRepository<KeywordDirect, Long> {

    Optional<KeywordDirect> findById(Long id);
    KeywordDirect findByExternalId(Long id);
    List<KeywordDirect> findByYandexUserAndMainStrategy(YandexUser yandexUser, Strategy mainStrategy);
}
