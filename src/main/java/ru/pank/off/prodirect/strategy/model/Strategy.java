package ru.pank.off.prodirect.strategy.model;

import lombok.Data;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "strategy")
@Data
public class Strategy {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String name;
    private String description;
    private String formula;
    @Column(name="direct_user_id", updatable = false)
    private Long directUserId;

    /*@JoinColumn(name = "direct_user_id", updatable = false, insertable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private DirectUser directUser;*/

    @OneToMany(mappedBy = "mainStrategy", fetch = FetchType.EAGER, cascade = CascadeType.REMOVE)
    private Set<KeywordDirect> keywordsByMainStrategy;
    @OneToMany(mappedBy = "secondStrategy", fetch = FetchType.EAGER, cascade = CascadeType.REMOVE)
    private Set<KeywordDirect> keywordsBySecondStrategy;


    @OneToMany(mappedBy = "mainStrategy", fetch = FetchType.EAGER, cascade = CascadeType.REMOVE)
    private Set<CampaignDirect> campaignsByMainStrategy;
    @OneToMany(mappedBy = "secondStrategy", fetch = FetchType.EAGER, cascade = CascadeType.REMOVE)
    private Set<CampaignDirect> campaignsBySecondStrategy;


    @OneToMany(mappedBy = "mainStrategy", fetch = FetchType.EAGER, cascade = CascadeType.REMOVE)
    private Set<AdGroupDirect> adGroupsByMainStrategy;
    @OneToMany(mappedBy = "secondStrategy", fetch = FetchType.EAGER, cascade = CascadeType.REMOVE)
    private Set<AdGroupDirect> adGroupsBySecondStrategy;

}
