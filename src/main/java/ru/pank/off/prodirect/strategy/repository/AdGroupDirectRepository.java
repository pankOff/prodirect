package ru.pank.off.prodirect.strategy.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.pank.off.prodirect.model.YandexUser;
import ru.pank.off.prodirect.strategy.model.AdGroupDirect;
import ru.pank.off.prodirect.strategy.model.Strategy;

import java.util.List;
import java.util.Optional;

public interface AdGroupDirectRepository extends JpaRepository<AdGroupDirect, Long> {
    Optional<AdGroupDirect> findById(Long id);
    AdGroupDirect findByExternalId(Long id);
    List<AdGroupDirect> findByYandexUserAndMainStrategy(YandexUser yandexUser, Strategy mainStrategy);
}