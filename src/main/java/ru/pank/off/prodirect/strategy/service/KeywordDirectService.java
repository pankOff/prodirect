package ru.pank.off.prodirect.strategy.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.pank.off.prodirect.model.YandexUser;
import ru.pank.off.prodirect.strategy.model.KeywordDirect;
import ru.pank.off.prodirect.strategy.model.Strategy;
import ru.pank.off.prodirect.strategy.repository.KeywordDirectRepository;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class KeywordDirectService {
    @Autowired
    private KeywordDirectRepository repository;

    public List<Long> getIdsByYandexUserAndMainStrategy(YandexUser yandexUser, Strategy mainStrategy){
        List<Long> idsForStrategy = repository.findByYandexUserAndMainStrategy(yandexUser, mainStrategy)
                .stream().map(KeywordDirect::getExternalId)
                .collect(Collectors.toList());
        return idsForStrategy;
    }

    public Optional<KeywordDirect> getById(Long keywordId) {
        return repository.findById(keywordId);
    }

    public KeywordDirect getByYandexId(Long yandexId) {
        return repository.findByExternalId(yandexId);
    }
}
