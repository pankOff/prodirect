package ru.pank.off.prodirect.strategy.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.pank.off.prodirect.model.YandexUser;
import ru.pank.off.prodirect.strategy.model.CampaignDirect;
import ru.pank.off.prodirect.strategy.model.Strategy;

import java.util.List;
import java.util.Optional;

public interface CampaignDirectRepository extends JpaRepository<CampaignDirect, Long> {
    Optional<CampaignDirect> findById(Long id);
    CampaignDirect findByExternalId(Long id);
    List<CampaignDirect> findByYandexUserAndMainStrategy(YandexUser yandexUser, Strategy mainStrategy);
}
