package ru.pank.off.prodirect.strategy.model;

import ru.pank.off.prodirect.model.YandexUser;

import javax.persistence.*;

@Entity
@Table(name = "campaign")
public class CampaignDirect {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private Long externalId;
    private Long mainStrategyMaxBid;
    private Long secondStrategyMaxBid;

    @ManyToOne(optional = false)
    @JoinColumn(name = "main_strategy_id")
    private Strategy mainStrategy;

    @ManyToOne(optional = false)
    @JoinColumn(name = "second_strategy_id")
    private Strategy secondStrategy;

    @ManyToOne(optional = false)
    @JoinColumn(name = "yandex_user_id")
    private YandexUser yandexUser;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getExternalId() {
        return externalId;
    }

    public void setExternalId(Long externalId) {
        this.externalId = externalId;
    }

    public Strategy getMainStrategy() {
        return mainStrategy;
    }

    public void setMainStrategy(Strategy mainStrategy) {
        this.mainStrategy = mainStrategy;
    }

    public Strategy getSecondStrategy() {
        return secondStrategy;
    }

    public void setSecondStrategy(Strategy secondStrategy) {
        this.secondStrategy = secondStrategy;
    }

    public YandexUser getYandexUser() {
        return yandexUser;
    }

    public void setYandexUser(YandexUser yandexUser) {
        this.yandexUser = yandexUser;
    }

    public Long getMainStrategyMaxBid() {
        return mainStrategyMaxBid;
    }

    public void setMainStrategyMaxBid(Long mainStrategyMaxBid) {
        this.mainStrategyMaxBid = mainStrategyMaxBid;
    }

    public Long getSecondStrategyMaxBid() {
        return secondStrategyMaxBid;
    }

    public void setSecondStrategyMaxBid(Long secondStrategyMaxBid) {
        this.secondStrategyMaxBid = secondStrategyMaxBid;
    }
}
