package ru.pank.off.prodirect.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import ru.pank.off.prodirect.model.DirectUser;
import ru.pank.off.prodirect.model.SelectionCriteria;
import ru.pank.off.prodirect.service.AdGroupService;
import ru.pank.off.prodirect.service.DirectUserService;

@RestController
public class AdGroupController {

    @Autowired
    private AdGroupService adGroupService;
    @Autowired
    private DirectUserService directUserService;

    @ResponseStatus(HttpStatus.OK)
    @PostMapping(value = "/getAdGroupsForCampaigns", produces = "application/json")
    @CrossOrigin(origins = "http://localhost:3000")
    public @ResponseBody
    String getAllAddGroupsForCampaign(@RequestHeader(value="Authorization") String directAuthToken,
                                      @RequestBody SelectionCriteria selectionCriteria) throws Exception {
        DirectUser directUser = directUserService.auth(directAuthToken);
        if(directUser != null) {
            String authToken = directUserService.getAuthTokenForYandexUserEmail(directUser, selectionCriteria.getYandexUserEmail());
            return adGroupService.getAllAddGroups(authToken, selectionCriteria.getCampaignsIds());
        } else {
            return "{\"Status\":\"FALSE\"}";
        }
    }
}
