package ru.pank.off.prodirect.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import ru.pank.off.prodirect.service.CampaignService;
@RestController
public class CampaignsController {

    @Autowired
    private CampaignService campaignService;

    @ResponseStatus(HttpStatus.OK)
    @PostMapping(value = "/getCampaigns", produces = "application/json")
    @CrossOrigin(origins = "http://localhost:3000")
    public @ResponseBody
    String getAllCampaigns(@RequestHeader(value="Authorization") String directAuthToken) throws Exception {
        return campaignService.getAllCampaignsForDirectUser(directAuthToken);
    }

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/getCampaignsAndUsers",  method = RequestMethod.GET, produces = "application/json")
    @CrossOrigin(origins = "http://localhost:3000")
    public @ResponseBody
    String getAllUsersAndCampaigns(@RequestHeader(value="Authorization") String directAuthToken) throws Exception {
        return campaignService.getAllCampaignsAndYandexUsersForDirectUser(directAuthToken);
    }


}
