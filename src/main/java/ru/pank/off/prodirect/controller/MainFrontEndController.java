package ru.pank.off.prodirect.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import ru.pank.off.prodirect.model.SelectionCriteria;
import ru.pank.off.prodirect.service.*;
import ru.pank.off.prodirect.service.impl.AdGroupServiceImpl;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/")
public class MainFrontEndController {
    @Autowired
    private CampaignService campaignService;
    @Autowired
    private AdGroupService adGroupService;
    @Autowired
    private AdGroupServiceImpl adGroupServiceImpl;
    @Autowired
    private KeywordService keywordService;
    @Autowired
    private TrafficService trafficService;
    @Autowired
    private BidService bidService;
    @Autowired
    private AdService adService;



    /**
     * get methods
     */

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/getComp", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    String getAllComp(@RequestHeader(value="Authorization") String authToken) throws Exception {
        return campaignService.getAllCampaigns(authToken);
    }

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/getConnection", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    String getTestConnection(@RequestHeader(value="Authorization") String authToken) {
        return "{"+authToken+"}";
    }

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/getAdGroups", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    String getAdGroups(@RequestHeader(value="Authorization") String authToken, @Param("campaignId") Long campaignId) throws Exception {
        List<Long> ids = new ArrayList<>();
        ids.add(campaignId);
        return adGroupService.getAllAddGroups(authToken, ids);
    }

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/getKeywords", method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody
    String getKeywords(@RequestHeader(value="Authorization") String authToken, @RequestBody SelectionCriteria selectionCriteria) throws Exception {
        return keywordService.getKeywords(authToken, selectionCriteria);
    }

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/getAds", method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody
    String getAds(@RequestHeader(value="Authorization") String authToken, @RequestBody SelectionCriteria selectionCriteria) throws Exception {
        return adService.getAds(authToken, selectionCriteria);
    }

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/getTraffic", method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody
    String getKeywordBids(@RequestHeader(value="Authorization") String authToken, @RequestBody SelectionCriteria selectionCriteria) throws Exception {
        return trafficService.getTraffic(authToken, selectionCriteria);
    }

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/getBids", method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody
    String getBids(@RequestHeader(value="Authorization") String authToken, @RequestBody SelectionCriteria selectionCriteria) throws Exception {
        return bidService.getBids(authToken, selectionCriteria);
    }

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/getKeywordsBids", method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody
    String getKeywordsBids(@RequestHeader(value="Authorization") String authToken, @RequestBody SelectionCriteria selectionCriteria) throws Exception {
        return adGroupService.getAllAddGroups(authToken, selectionCriteria.getCampaignsIds());
    }

    /**
     * set methods
     */



    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/setBids", method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody
    String setBids(@RequestHeader(value="Authorization") String authToken, @RequestBody SelectionCriteria selectionCriteria) throws Exception {
        return bidService.setBid(authToken, selectionCriteria);
    }

    /*@ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/setKeywordBidForKeyword", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    String setKeywordBidForKeyword(@Param("bid") Long bid, @Param("keywordId") Long id) throws Exception {
        return trafficService.setBidForKeyword(bid, id);
    }

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/setKeywordBidForCampaigns", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    String setKeywordBidForCampaigns(@Param("bid") Long bid, @Param("CampaignId") Long id) throws Exception {
        return trafficService.setBidForKeyword(bid, id);
    }*/
}
