package ru.pank.off.prodirect.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import ru.pank.off.prodirect.model.DirectUser;
import ru.pank.off.prodirect.model.YandexUser;
import ru.pank.off.prodirect.service.DirectUserService;
import ru.pank.off.prodirect.service.YandexUserService;
import thirdparty.contextguide.yandexservices.exceptions.ApiResponseException;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@RestController
public class AuthController {

    @Autowired
    private DirectUserService directUserService;
    @Autowired
    private YandexUserService yandexUserService;


    @ResponseStatus(HttpStatus.OK)
    @PostMapping(value = "/addNewDirectUser", produces = "application/json")
    @CrossOrigin(origins = "http://localhost:3000")
    public @ResponseBody
    String addNewDirectUser(@RequestBody DirectUser directUserValue){
        return directUserService.addDirectUser(directUserValue);
    }

    @ResponseStatus(HttpStatus.OK)
    @PostMapping(value = "/login", produces = "application/json")
    @CrossOrigin(origins = "http://localhost:3000")
    public @ResponseBody
    String login(@RequestBody DirectUser directUserValue) {
        return directUserService.login(directUserValue);
    }

    @ResponseStatus(HttpStatus.OK)
    @PostMapping (value = "/addYandexUser", produces = "application/json")
    @CrossOrigin(origins = "http://localhost:3000")
    public @ResponseBody
    String addNewYandexUser(@RequestHeader(value="Authorization") String directAuthToken,
            @RequestBody YandexUser userValue){
        return yandexUserService.addYandexUser(directAuthToken, userValue);
    }

    @ResponseStatus(HttpStatus.OK)
    @PostMapping(value = "/getYandexUsers", produces = "application/json")
    @CrossOrigin(origins = "http://localhost:3000")
    public @ResponseBody
    String getYandexUsers(@RequestHeader(value="Authorization") String directAuthToken) {
        return directUserService.getYandexUsers(directAuthToken);
    }

}
