package ru.pank.off.prodirect.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import ru.pank.off.prodirect.model.DirectUser;
import ru.pank.off.prodirect.model.SelectedEntity;
import ru.pank.off.prodirect.service.DirectUserService;
import ru.pank.off.prodirect.service.StrategyService;
import ru.pank.off.prodirect.strategy.model.Strategy;
import thirdparty.contextguide.yandexservices.exceptions.ApiResponseException;

import java.io.IOException;

@RestController
public class StrategyController {

    @Autowired
    private StrategyService strategyService;

    @Autowired
    private DirectUserService directUserService;

    @ResponseStatus(HttpStatus.OK)
    @PostMapping(value = "/setStrategy", produces = "application/json")
    @CrossOrigin(origins = "http://localhost:3000")
    public @ResponseBody
    String setStrategy(@RequestHeader(value="Authorization") String directAuthToken,
                       @RequestBody SelectedEntity selectedEntityValue
                       ) throws IOException, ApiResponseException {
        return strategyService.setEntitiesForStrategy(directAuthToken, selectedEntityValue);
    }

    @ResponseStatus(HttpStatus.OK)
    @PostMapping(value = "/saveStrategy", produces = "application/json")
    @CrossOrigin(origins = "http://localhost:3000")
    public @ResponseBody
    String saveStrategy(@RequestHeader(value="Authorization") String directAuthToken) throws IOException, ApiResponseException {
        return strategyService.getStrategies();
    }

    @ResponseStatus(HttpStatus.OK)
    @PostMapping(value = "/getStrategies", produces = "application/json")
    @CrossOrigin(origins = "http://localhost:3000")
    public @ResponseBody
    String getStrategies(@RequestHeader(value="Authorization") String directAuthToken) throws IOException, ApiResponseException {
        return strategyService.getStrategies();
    }

    @ResponseStatus(HttpStatus.OK)
    @PostMapping(value = "/strategy/create", produces = "application/json")
    @CrossOrigin(origins = "http://localhost:3000")
    public void createStrategy(@RequestHeader(value = "Authorization") String directAuthToken,
                               @RequestBody Strategy strategy) {
        DirectUser directUser = directUserService.auth(directAuthToken);
        strategyService.create(strategy, directUser.getId());
    }

    @ResponseStatus(HttpStatus.OK)
    @PutMapping(value = "/strategy/update", produces = "application/json")
    @CrossOrigin(origins = "http://localhost:3000")
    public void updateStrategy(@RequestHeader(value = "Authorization") String directAuthToken,
                               @RequestBody Strategy strategy) {
        DirectUser directUser = directUserService.auth(directAuthToken);
        strategyService.update(strategy, directUser.getId());
    }

    @ResponseStatus(HttpStatus.OK)
    @DeleteMapping(value = "/strategy/delete/{id}", produces = "application/json")
    @CrossOrigin(origins = "http://localhost:3000")
    public void deleteStrategy(@RequestHeader(value = "Authorization") String directAuthToken,
                               @PathVariable Long id) {
        DirectUser directUser = directUserService.auth(directAuthToken);
        strategyService.delete(id);
    }

}
