package ru.pank.off.prodirect;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import ru.pank.off.prodirect.scheduler.DirectScheduler;
import de.dentrassi.crypto.pem.PemKeyStoreProvider;

import java.security.Security;
import java.sql.SQLException;

@SpringBootApplication
public class Application {

    public static void main(String[] args) throws SQLException {
        //SpringApplication.run(Application.class, args);
        Security.addProvider(new PemKeyStoreProvider());
        ConfigurableApplicationContext context = SpringApplication.run(Application.class, args);
        //TODO: think about: how to start registerAll() after context initialization
        /*ListenersRegistry registry = context.getBean(ListenersRegistry.class);
        registry.registerAll();
        ChannelsPoller poller = new ChannelsPoller(registry);
        poller.startPolling();*/




        DirectScheduler directScheduler = context.getBean(DirectScheduler.class);
        directScheduler.setContext(context);
        directScheduler.start();
    }
}
