package ru.pank.off.prodirect.model;


import ru.pank.off.prodirect.strategy.model.AdGroupDirect;
import ru.pank.off.prodirect.strategy.model.CampaignDirect;
import ru.pank.off.prodirect.strategy.model.KeywordDirect;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "yandex_user")
public class YandexUser {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String username;
    private String email;
    private String yandexAuthToken;

    @ManyToOne(optional = false)
    @JoinColumn(name = "direct_user_id")
    private DirectUser manager;

    @OneToMany(mappedBy = "yandexUser", fetch = FetchType.EAGER, cascade = CascadeType.REMOVE)
    private Set<KeywordDirect> keywords;

    @OneToMany(mappedBy = "yandexUser", fetch = FetchType.EAGER, cascade = CascadeType.REMOVE)
    private Set<CampaignDirect> campaigns;

    @OneToMany(mappedBy = "yandexUser", fetch = FetchType.EAGER, cascade = CascadeType.REMOVE)
    private Set<AdGroupDirect> adGroups;

    public YandexUser() {}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public DirectUser getManager() {
        return manager;
    }

    public void setManager(DirectUser manager) {
        this.manager = manager;
    }

    public String getYandexAuthToken() {
        return yandexAuthToken;
    }

    public void setYandexAuthToken(String yandexAuthToken) {
        this.yandexAuthToken = yandexAuthToken;
    }

    public Set<KeywordDirect> getKeywords() {
        return keywords;
    }

    public void setKeywords(Set<KeywordDirect> keywords) {
        this.keywords = keywords;
    }

    public Set<CampaignDirect> getCampaigns() {
        return campaigns;
    }

    public void setCampaigns(Set<CampaignDirect> campaigns) {
        this.campaigns = campaigns;
    }

    public Set<AdGroupDirect> getAdGroups() {
        return adGroups;
    }

    public void setAdGroups(Set<AdGroupDirect> adGroups) {
        this.adGroups = adGroups;
    }
}
