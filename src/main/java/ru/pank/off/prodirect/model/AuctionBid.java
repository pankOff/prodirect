package ru.pank.off.prodirect.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import ru.pank.off.prodirect.antlr.VariableMap;

@Data
public class AuctionBid {

    @JsonProperty("Position")
    private VariableMap.AuctionBidName position;
    @JsonProperty("Bid")
    private Double bid;
    @JsonProperty("Price")
    private Double price;

}