package ru.pank.off.prodirect.model;


import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.util.Collection;
import java.util.Set;

@Entity
@Data
@NoArgsConstructor
@Table(name = "direct_user")
public class DirectUser implements UserDetails {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    //@NotBlank(message = "Username cannot be empty")
    private String username;

    //@NotBlank(message = "Password cannot be empty")
    private String password;

    private boolean active;

    //@Email(message = "Email is not correct")
    //@NotBlank(message = "Email cannot be empty")
    private String email;

    private String directAuthToken;

    /*private String activationCode;*/

    @ElementCollection(targetClass = Role.class, fetch = FetchType.EAGER)
    @CollectionTable(name = "user_role", joinColumns = @JoinColumn(name = "user_id"))
    @Enumerated(EnumType.STRING)
    private Set<Role> roles;

    @OneToMany(mappedBy = "manager", fetch = FetchType.EAGER, cascade = CascadeType.REMOVE)
    private Set<YandexUser> yandexUsers;

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return isActive();
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return getRoles();


    }

    public boolean isAdmin() {
        return roles.contains(Role.ADMIN);
    }

    /*public String getActivationCode() {
        return activationCode;
    }

    public void setActivationCode(String activationCode) {
        this.activationCode = activationCode;
    }*/

}
