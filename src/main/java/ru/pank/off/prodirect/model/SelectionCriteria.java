package ru.pank.off.prodirect.model;

import ru.pank.off.prodirect.config.APIRequestConfig;

import java.util.List;

public class SelectionCriteria {
    private List<Long> campaignsIds;
    private List<Long> adGroupIds;
    private List<Long> keywordIds;
    private Long bid;
    private String yandexUserEmail;

    public List<Long> getAdGroupIds() {
        return adGroupIds;
    }

    public List<Long> getCampaignsIds() {
        return campaignsIds;
    }

    public List<Long> getKeywordIds() {
        return keywordIds;
    }

    public Long getBid() { return bid; }

    public void setAdGroupIds(List<Long> adGroupIds) {
        this.adGroupIds = adGroupIds;
    }

    public void setCampaignsIds(List<Long> campaignsIds) {
        this.campaignsIds = campaignsIds;
    }

    public void setKeywordIds(List<Long> keywordIds) {
        this.keywordIds = keywordIds;
    }

    public void setBid( Long bid) { this.bid = bid; }

    public String getYandexUserEmail() {
        return yandexUserEmail;
    }

    public void setYandexUserEmail(String yandexUserEmail) {
        this.yandexUserEmail = yandexUserEmail;
    }

    public String toString(String entity){
        switch (entity) {
            case APIRequestConfig.ADGROUP_GET:
                return "\"SelectionCriteria\":{ \"CampaignIds\": "
                        +this.getCampaignsIds() +"}";
            case APIRequestConfig.KEYWORDS_GET:
                return "\"SelectionCriteria\":{ \"CampaignIds\": "
                        +this.getCampaignsIds()
                        +",\"AdGroupIds\": "
                        +this.getAdGroupIds()+"}";
            case APIRequestConfig.ADS_GET:
                return "\"SelectionCriteria\":{ \"CampaignIds\": "
                        +this.getCampaignsIds()
                        +",\"AdGroupIds\": "
                        +this.getAdGroupIds()+"}";
            case APIRequestConfig.TRAFFIC_GET:
                return "\"SelectionCriteria\":{ \"CampaignIds\": "+this.getCampaignsIds()
                        +",\"AdGroupIds\": "+this.getAdGroupIds()
                        +",\"KeywordIds\": "+this.getKeywordIds()+"}";
            case APIRequestConfig.BIDS_GET:
                return "\"SelectionCriteria\":{ \"CampaignIds\": "+this.getCampaignsIds()
                        +",\"AdGroupIds\": "+this.getAdGroupIds()
                        +",\"KeywordIds\": "+this.getKeywordIds()+"}";
            case APIRequestConfig.BIDS_SET: {
                String campaign,adGroup,keyword = null;
                if(this.getCampaignsIds().size() > 0){
                    campaign = this.getCampaignsIds().get(0).toString();
                    return "\"Bids\":[{ \"CampaignId\": " + campaign;
                }
                if(this.getAdGroupIds().size() > 0){
                    adGroup = this.getAdGroupIds().get(0).toString();
                    return "\"Bids\":[{ \"AdGroupId\": " + adGroup;
                }
                if(this.getKeywordIds().size() > 0){
                    keyword = this.getKeywordIds().get(0).toString();
                    return "\"Bids\":[{\"KeywordId\": " + keyword;
                }
            }
            default: return "";
        }
    }

    public static String emptySelectionCriteriaToString() {
        return "\"SelectionCriteria\":{}";
    }
}
