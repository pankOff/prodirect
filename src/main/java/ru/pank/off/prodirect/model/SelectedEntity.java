package ru.pank.off.prodirect.model;

import ru.pank.off.prodirect.strategy.model.Strategy;

import java.util.List;

public class SelectedEntity {

    private List<Long> campaigns;
    private List<Long> adGroups;
    private List<Long> keywords;

    private Strategy mainStrategy;
    private Strategy secondStrategy;
    private YandexUser yandexUser;

    private Long mainStrategyMaxBid;
    private Long secondStrategyMaxBid;

    public List<Long> getCampaigns() {
        return campaigns;
    }

    public void setCampaigns(List<Long> campaigns) {
        this.campaigns = campaigns;
    }

    public List<Long> getAdGroups() {
        return adGroups;
    }

    public void setAdGroups(List<Long> adGroups) {
        this.adGroups = adGroups;
    }

    public List<Long> getKeywords() {
        return keywords;
    }

    public void setKeywords(List<Long> keywords) {
        this.keywords = keywords;
    }

    public Strategy getMainStrategy() {
        return mainStrategy;
    }

    public void setMainStrategy(Strategy mainStrategy) {
        this.mainStrategy = mainStrategy;
    }

    public Strategy getSecondStrategy() {
        return secondStrategy;
    }

    public void setSecondStrategy(Strategy secondStrategy) {
        this.secondStrategy = secondStrategy;
    }

    public YandexUser getYandexUser() {
        return yandexUser;
    }

    public void setYandexUser(YandexUser yandexUser) {
        this.yandexUser = yandexUser;
    }

    public Long getMainStrategyMaxBid() {
        return mainStrategyMaxBid;
    }

    public void setMainStrategyMaxBid(Long mainStrategyMaxBid) {
        this.mainStrategyMaxBid = mainStrategyMaxBid;
    }

    public Long getSecondStrategyMaxBid() {
        return secondStrategyMaxBid;
    }

    public void setSecondStrategyMaxBid(Long secondStrategyMaxBid) {
        this.secondStrategyMaxBid = secondStrategyMaxBid;
    }
}
