package ru.pank.off.prodirect.scheduler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.stereotype.Service;
import ru.pank.off.prodirect.strategy.model.Strategy;
import ru.pank.off.prodirect.strategy.repository.StrategyRepository;
import ru.pank.off.prodirect.strategy.service.StrategyExecutor;

@Service
public class DirectTaskExecutor {

    private ConfigurableApplicationContext context;
    private StrategyRepository strategyRepository;

    @Autowired
    private StrategyExecutor strategyExecutor;

    public void execute() {
        for(Strategy strategy : strategyRepository.findAll()) {
            //StrategyExecutor strategyExecutor = StrategySelector.getStrategyExecutor(strategy.getName());
            try {
                strategyExecutor.setContext(context);
                strategyExecutor.execute(strategy);
            } catch (Exception e) {
                System.out.println("DirectTaskExecutor ERROR" + e.getMessage() + e);
            }
        }
    }

    public ConfigurableApplicationContext getContext() {
        return context;
    }

    public void setContext(ConfigurableApplicationContext context) {
        this.context = context;
        setStrategyRepository(context.getBean(StrategyRepository.class));
    }

    public StrategyRepository getStrategyRepository() {
        return strategyRepository;
    }

    public void setStrategyRepository(StrategyRepository strategyRepository) {
        this.strategyRepository = strategyRepository;
    }
}
