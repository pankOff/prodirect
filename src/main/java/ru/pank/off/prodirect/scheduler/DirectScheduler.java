package ru.pank.off.prodirect.scheduler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

@Component
public class DirectScheduler {

    private static List<ScheduledFuture<Void>> tasks = new ArrayList<>();
    private ScheduledThreadPoolExecutor scheduledThreadPoolExecutor = new ScheduledThreadPoolExecutor(500);
    private ConfigurableApplicationContext context;

    public void start(){
        //Comment this line to stop set bids to Yandex
        //scheduledThreadPoolExecutor.scheduleAtFixedRate(new DirectTask(context), 0, 5, TimeUnit.MINUTES);
    }


    public ConfigurableApplicationContext getContext() {
        return context;
    }

    public void setContext(ConfigurableApplicationContext context) {
        this.context = context;
    }
}
