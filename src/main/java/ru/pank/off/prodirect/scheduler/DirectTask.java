package ru.pank.off.prodirect.scheduler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.stereotype.Service;

@Service
public class DirectTask implements Runnable {

    @Autowired
    private DirectTaskExecutor directTaskExecutor;

    private ConfigurableApplicationContext context;

    DirectTask(ConfigurableApplicationContext context){
        this.context = context;
    }

    @Override
    public void run() {
        DirectTaskExecutor directTaskExecutor = context.getBean(DirectTaskExecutor.class);
        directTaskExecutor.setContext(context);
        directTaskExecutor.execute();
    }
}
