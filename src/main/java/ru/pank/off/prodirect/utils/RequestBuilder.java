package ru.pank.off.prodirect.utils;

import ru.pank.off.prodirect.config.APIRequestConfig;
import ru.pank.off.prodirect.model.SelectionCriteria;
import thirdparty.contextguide.yandexservices.exceptions.ApiResponseException;

import java.util.HashMap;
import java.util.Map;

public class RequestBuilder {

    public static String getBody(String entity,SelectionCriteria selectionCriteria) throws ApiResponseException {
        switch (entity){
            case APIRequestConfig.ADGROUP_GET : return Body.getAdGroups(selectionCriteria);
            case APIRequestConfig.KEYWORDS_GET : return Body.getKeywords(selectionCriteria);
            case APIRequestConfig.ADS_GET : return Body.getAds(selectionCriteria);
            case APIRequestConfig.TRAFFIC_GET : return Body.getTraffic(selectionCriteria);
            case APIRequestConfig.BIDS_GET : return Body.getBids(selectionCriteria);
            case APIRequestConfig.BIDS_SET : return Body.setBid(selectionCriteria);
            default: return "";
        }
    }

    public static String getBody(String entity) throws ApiResponseException {
        switch (entity){
            case APIRequestConfig.CAMPAIGN_GET : return Body.getCampaigns();
            default: return "";
        }
    }

    public static Map<String, String> getHeaders(String authToken){
        Map<String, String> headers = new HashMap<>();
        headers.put("Authorization", "Bearer "+ authToken);
        headers.put("content-type", "application/json");
        return headers;
    }

    private static final class Body{
        private static final String id = "\"Id\"";
        private static final String name = "\"Name\"";
        private static final String bid = "\"Bid\"";
        private static final String currentSearchPrice = "\"CurrentSearchPrice\"";
        private static final String textAd = "\"TextAd\"";
        private static final String keyword = "\"Keyword\"";
        private static final String keywordId = "\"KeywordId\"";
        private static final String adGroupId ="\"AdGroupId\"";
        private static final String campaignId ="\"CampaignId\"";
        private static final String auctionBidsId ="\"AuctionBids\"";
        private static final String text = "\"Text\"";
        private static final String title = "\"Title\"";
        private static final String title2 = "\"Title2\"";
        private static final String status = "\"Status\"";
        private static final String state = "\"State\"";
        private static final String head = "{\"method\": ";
        private static final String getMethod = "\"get\",";
        private static final String setMethod = "\"set\",";
        private static final String params = "\"params\": { ";
        private static final String fieldNames = "\"FieldNames\":[";
        private static final String textAdFieldNames = "\"TextAdFieldNames\":[";
        private static final String searchFieldNames = "\"SearchFieldNames\": [\"Bid\",\"AuctionBids\"]";
        private static final String footer = "}}";

        private static final String getCampaigns() {
            String body = head+getMethod+params+SelectionCriteria.emptySelectionCriteriaToString()+","+fieldNames
                    +id+","
                    +name+","
                    +status+","
                    +state
                    +"]"+footer;
            return body;
        }

        public static String getAdGroups(SelectionCriteria selectionCriteria) {
            String body = head+getMethod+params+selectionCriteria.toString(APIRequestConfig.ADGROUP_GET)+","+fieldNames
                    +id+","
                    +name
                    +"]"+footer;
            return body;
        }


        private static final String getKeywords(SelectionCriteria selectionCriteria) throws ApiResponseException {
            String body = null;
            if (selectionCriteria.getCampaignsIds() == null && selectionCriteria.getAdGroupIds() == null && selectionCriteria.getKeywordIds() == null) {
                throw new ApiResponseException("campaignIds or adGroupIds or adKeywordIds must be set");
            } else {
                body =head+getMethod+params+selectionCriteria.toString(APIRequestConfig.KEYWORDS_GET)+","+fieldNames+keyword+","+id+","+adGroupId+","+campaignId+"]"+footer;
            }
            return body;
        }

        private static final String getAds(SelectionCriteria selectionCriteria) throws ApiResponseException {
            String body = null;
            if (selectionCriteria.getCampaignsIds() == null && selectionCriteria.getAdGroupIds() == null) {
                throw new ApiResponseException("campaignIds or adGroupIds or adKeywordIds must be set");
            } else {
                body =head+getMethod+params+selectionCriteria.toString(APIRequestConfig.ADS_GET)+","+fieldNames+id+","+adGroupId+","+status+"],"
                        +textAdFieldNames+text+","+title+","+title2
                        +"]"
                        +footer;
            }
            return body;
        }

        private static final String getTraffic(SelectionCriteria selectionCriteria) throws ApiResponseException {
            String body = null;
            if (selectionCriteria.getCampaignsIds() == null && selectionCriteria.getAdGroupIds() == null && selectionCriteria.getKeywordIds() == null) {
                throw new ApiResponseException("campaignIds or adGroupIds or adKeywordIds must be set");
            } else {
                body =head+getMethod+params+selectionCriteria.toString(APIRequestConfig.TRAFFIC_GET)+","+fieldNames+keywordId+","+adGroupId+","+campaignId+"],"+searchFieldNames+footer;
            }
            return body;
        }

        private static final String getBids(SelectionCriteria selectionCriteria) throws ApiResponseException {
            String body = null;
            if (selectionCriteria.getCampaignsIds() == null && selectionCriteria.getAdGroupIds() == null && selectionCriteria.getKeywordIds() == null) {
                throw new ApiResponseException("campaignIds or adGroupIds or adKeywordIds must be set");
            } else {
                body =head+getMethod+params+selectionCriteria.toString(APIRequestConfig.BIDS_GET)+","+fieldNames+keywordId+","+adGroupId+","+campaignId+","+auctionBidsId+","+bid+","+currentSearchPrice+"]"+footer;
            }
            return body;
        }

        private static final String setBid(SelectionCriteria selectionCriteria) throws ApiResponseException {
            String body = null;
            if (selectionCriteria.getCampaignsIds() == null && selectionCriteria.getAdGroupIds() == null && selectionCriteria.getKeywordIds() == null) {
                throw new ApiResponseException("campaignIds or adGroupIds or adKeywordIds must be set");
            } else {
                if (selectionCriteria.getBid() == null) {
                    throw new ApiResponseException("there is no bid");
                } else {
                    body = head + setMethod + params + selectionCriteria.toString(APIRequestConfig.BIDS_SET) + ",\"Bid\":" + selectionCriteria.getBid() + "}]" + footer;
                }
            }
            return body;
        }
    }

}
