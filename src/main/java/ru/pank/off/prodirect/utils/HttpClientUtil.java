package ru.pank.off.prodirect.utils;

import org.apache.commons.io.IOUtils;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicHeader;
import org.apache.http.util.EntityUtils;
import org.springframework.http.HttpMethod;

import java.util.Map;

public class HttpClientUtil {

    public static CloseableHttpClient client = HttpClients.createDefault();


    /**
     * Sends HTTP GET/POST requests
     *
     * @param method can be only {@code HttpMethod.GET} or {@code HttpMethod.POST}
     * @return String representation of response body if the status is OK 200
     * @throws Exception if the returned status code is not OK 200 and if
     *                   {@code method} is not POST or GET or IOException is thrown by
     */
    public static String sendRequest(String url, HttpMethod method,
                                     Map<String, String> headers, String body) throws Exception {
        HttpRequestBase request;

        switch (method) {
            case POST:
                request = new HttpPost(url);
                ((HttpPost) request).setEntity(new StringEntity(body));
                break;
            case GET:
                request = new HttpGet(url);
                break;
            default:
                throw new Exception("Only POST and GET methods are supported");
        }

        request.setHeaders(getHeadersFromMap(headers));
        //TODO: Handle IOException somehow... Now it's delegated to higher level
        CloseableHttpResponse response = client.execute(request);
        try {
            if (response.getStatusLine().getStatusCode() == 200) {
                HttpEntity entity = response.getEntity();
                String responseStr = IOUtils.toString(entity.getContent(), "utf-8");
                EntityUtils.consume(entity);
                return responseStr;
            } else throw new Exception("Invalid status code: " + response.getStatusLine().getStatusCode());
        } finally {
            response.close();
        }
    }

    /**
     * Converts {@code Map<String, String>} to {@link org.apache.http.Header Header[]}
     *
     * @param map {@link java.util.Map Map<String, String>} with
     *            key as header's name and value as the header's value
     * @return {@link org.apache.http.Header Header[]}
     */

    public static Header[] getHeadersFromMap(Map<String, String> map) {
        Header[] headers = new Header[map.size()];
        int i = 0;
        for (String key : map.keySet()) {
            headers[i] = new BasicHeader(key, map.get(key));
            i++;
        }
        return headers;
    }
}
