package thirdparty.contextguide.ad;

import thirdparty.contextguide.yandexservices.utils.JsonSerializableObject;

/**
 * Параметры графического объявления (при добавлении в группу для рекламы мобильных приложений).
 */
public class MobileAppImageAdAdd implements JsonSerializableObject {
//TODO

    @Override
    public String toString() {
        return this.toJson();
    }

}
