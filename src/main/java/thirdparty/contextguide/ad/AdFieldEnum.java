package thirdparty.contextguide.ad;

public enum AdFieldEnum {
    AdCategories,
    AgeLabel,
    AdGroupId,
    CampaignId,
    Id,
    State,
    Status,
    StatusClarification,
    Type,
    Subtype
}
