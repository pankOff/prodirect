package thirdparty.contextguide.yandexservices;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import thirdparty.contextguide.yandexservices.adgroups.AdGroups;
import thirdparty.contextguide.yandexservices.adgroups.AdGroupsDefaultImpl;
import thirdparty.contextguide.yandexservices.ads.Ads;
import thirdparty.contextguide.yandexservices.ads.AdsDefaultImpl;
import thirdparty.contextguide.yandexservices.campaigns.Campaigns;
import thirdparty.contextguide.yandexservices.campaigns.CampaignsDefaultImpl;
import thirdparty.contextguide.yandexservices.changes.Changes;
import thirdparty.contextguide.yandexservices.changes.ChangesDefaultImpl;
import thirdparty.contextguide.yandexservices.utils.DefaultJsonParser;
import thirdparty.contextguide.yandexservices.utils.JsonParser;
import thirdparty.contextguide.yandexservices.utils.ServiceConnectionManager;
import thirdparty.contextguide.yandexservices.utils.ServiceConnectionManagerDefaultImpl;


/**
 * Менеджер сервисов яндекса по умолчанию
 */
public class YandexServicesManagerDefaultImpl implements YandexServicesManager {
    private static final Logger log = LoggerFactory.getLogger(YandexServicesManagerDefaultImpl.class);
    private final JsonParser jsonParser = new DefaultJsonParser();
    private final ServiceConnectionManager serviceConnectionManager = new ServiceConnectionManagerDefaultImpl();

    @Override
    public AdGroups getAdGroupsService() {
        return new AdGroupsDefaultImpl(jsonParser, serviceConnectionManager);
    }

    @Override
    public Ads getAdsService() {
        return new AdsDefaultImpl(jsonParser, serviceConnectionManager);
    }

    @Override
    public Campaigns getCampaignsService() {
        return new CampaignsDefaultImpl(jsonParser, serviceConnectionManager);
    }

    @Override
    public Changes getChangesService() {
        return new ChangesDefaultImpl(jsonParser, serviceConnectionManager);
    }
}
