package thirdparty.contextguide.yandexservices;


import thirdparty.contextguide.yandexservices.adgroups.AdGroups;
import thirdparty.contextguide.yandexservices.ads.Ads;
import thirdparty.contextguide.yandexservices.campaigns.Campaigns;
import thirdparty.contextguide.yandexservices.changes.Changes;

public interface YandexServicesManager {
    AdGroups getAdGroupsService();

    Ads getAdsService();

    Campaigns getCampaignsService();

    Changes getChangesService();
}
