package thirdparty.contextguide.yandexservices.utils;


import org.apache.http.client.methods.HttpPost;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import thirdparty.contextguide.yandexservices.ServiceMethod;

import java.io.IOException;

import static jdk.nashorn.api.scripting.ScriptObjectMirror.wrap;


public class ServiceConnectionManagerDefaultImpl implements ServiceConnectionManager {
    private static final Logger log = LoggerFactory.getLogger(ServiceConnectionManagerDefaultImpl.class);
    private final String oauthToken;
    private final LocalHttpClient localHttpClient;


    public ServiceConnectionManagerDefaultImpl() {

        oauthToken = System.getProperty("token");
        localHttpClient = new LocalHttpClient();
    }

    public ServiceConnectionManagerDefaultImpl(String oauthToken) {
        //TODO delete this part
        //oauthToken = System.getProperty("token");
        this.oauthToken = oauthToken;
        localHttpClient = new LocalHttpClient();
    }

    @Override
    public String sendRequest(ServiceMethod method, String apiUrl, JsonSerializableObject jsonRequest) throws IOException {
        StringBuilder sb = new StringBuilder();
        sb.append(String.format("{\"method\": %s,\"params\":", wrap(String.valueOf(method), '"')));

        String addRequestJson = jsonRequest.toJson();
        sb.append(addRequestJson);
        sb.append('}');
        String finalJson = sb.toString();
        System.out.println(finalJson);
        HttpPost request = new PostApiRequest(method, apiUrl, oauthToken, finalJson).getHttpPost();

        return localHttpClient.execute(request);
    }


}
