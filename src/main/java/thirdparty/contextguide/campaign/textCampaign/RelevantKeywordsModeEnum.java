package thirdparty.contextguide.campaign.textCampaign;

/**
 * Режим подбора дополнительных релевантных фраз: MINIMUM, OPTIMAL или MAXIMUM
 */
public enum RelevantKeywordsModeEnum {
    MINIMUM,
    OPTIMAL,
    MAXIMUM
}