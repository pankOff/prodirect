package thirdparty.contextguide.adgroup;

/**
 * Статус группы
 */
public enum AdGroupStatusSelectionEnum {

    DRAFT, MODERATION, PREACCEPTED, ACCEPTED, REJECTED
}
