package thirdparty.contextguide.adgroup;

enum AdgroupFieldNames {
    Id, CampaignId, Status, Name, RegionIds, NegativeKeywords, TrackingParams, Type, Subtype
}