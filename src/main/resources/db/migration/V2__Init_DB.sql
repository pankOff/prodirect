create sequence hibernate_sequence start 1 increment 1;
create table ad_group (
    id int8 not null,
    external_id int8,
    main_strategy_max_bid int8,
    second_strategy_max_bid int8,
    main_strategy_id int8 not null,
    second_strategy_id int8 not null,
    yandex_user_id int8 not null,
    primary key (id));

create table campaign (
    id int8 not null,
    external_id int8,
    main_strategy_max_bid int8,
    second_strategy_max_bid int8,
    main_strategy_id int8 not null,
    second_strategy_id int8 not null,
    yandex_user_id int8 not null,
    primary key (id));

create table direct_user (
    id int8 not null,
    active boolean not null,
    direct_auth_token varchar(255),
    email varchar(255),
    password varchar(255),
    username varchar(255),
    primary key (id));

create table keyword (
    id int8 not null,
    external_id int8,
    main_strategy_max_bid int8,
    second_strategy_max_bid int8,
    main_strategy_id int8 not null,
    second_strategy_id int8 not null,
    yandex_user_id int8 not null,
    primary key (id));

create table strategy (
    id int8 not null,
    description varchar(255),
    name varchar(255),
    direct_user_id int8 not null,
    formula varchar(1000),
    primary key (id));

create table user_role (
    user_id int8 not null,
    roles varchar(255));

create table yandex_user (
    id int8 not null,
    email varchar(255),
    username varchar(255),
    yandex_auth_token varchar(255),
    direct_user_id int8 not null,
    primary key (id));

alter table if exists ad_group add constraint ad_group_main_strategy_id_fk foreign key (main_strategy_id) references strategy;
alter table if exists ad_group add constraint ad_group_second_strategy_id_fk foreign key (second_strategy_id) references strategy;
alter table if exists ad_group add constraint ad_group_yandex_user_id_fk foreign key (yandex_user_id) references yandex_user;


alter table if exists campaign add constraint campaign_main_strategy_id_fk foreign key (main_strategy_id) references strategy;
alter table if exists campaign add constraint campaign_second_strategy_id_fk foreign key (second_strategy_id) references strategy;
alter table if exists campaign add constraint campaign_yandex_user_id_fk foreign key (yandex_user_id) references yandex_user;


alter table if exists keyword add constraint keyword_main_strategy_id_fk foreign key (main_strategy_id) references strategy;
alter table if exists keyword add constraint keyword_second_strategy_id_fk foreign key (second_strategy_id) references strategy;
alter table if exists keyword add constraint keyword_yandex_user_id_fk foreign key (yandex_user_id) references yandex_user;


alter table if exists user_role add constraint role_direct_user_fk foreign key (user_id) references direct_user;
alter table if exists yandex_user add constraint yandex_user_direct_user_fk foreign key (direct_user_id) references direct_user;