alter table if exists ad_group drop constraint if exists ad_group_main_strategy_id_fk;
alter table if exists ad_group drop constraint if exists ad_group_second_strategy_id_fk;
alter table if exists ad_group drop constraint if exists ad_group_yandex_user_id_fk;


alter table if exists campaign drop constraint if exists campaign_main_strategy_id_fk;
alter table if exists campaign drop constraint if exists campaign_second_strategy_id_fk;
alter table if exists campaign drop constraint if exists campaign_yandex_user_id_fk;


alter table if exists keyword drop constraint if exists keyword_main_strategy_id_fk;
alter table if exists keyword drop constraint if exists keyword_second_strategy_id_fk;
alter table if exists keyword drop constraint if exists keyword_yandex_user_id_fk;


alter table if exists user_role drop constraint if exists role_direct_user_fk;
alter table if exists yandex_user drop constraint if exists yandex_user_direct_user_fk;

drop table if exists ad_group cascade;
drop table if exists campaign cascade;
drop table if exists direct_user cascade;
drop table if exists keyword cascade;
drop table if exists strategy cascade;
drop table if exists user_role cascade;
drop table if exists yandex_user cascade;

drop sequence if exists hibernate_sequence;