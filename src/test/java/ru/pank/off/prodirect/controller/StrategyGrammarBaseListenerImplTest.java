package ru.pank.off.prodirect.controller;

import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.junit.jupiter.api.Test;
import ru.pank.off.prodirect.antlr.StrategyGrammarBaseListenerImpl;
import ru.pank.off.prodirect.antlr.StrategyGrammarLexer;
import ru.pank.off.prodirect.antlr.StrategyGrammarParser;
import ru.pank.off.prodirect.antlr.VariableMap;

import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;

import static org.antlr.v4.runtime.CharStreams.fromFileName;
import static org.springframework.test.util.AssertionErrors.assertEquals;

class StrategyGrammarBaseListenerImplTest {

    @Test
    public void testParseSmoke_onlyNumbers() throws IOException {
        CharStream charStream = fromFileName("src/test/resources/parseSmokeTest.txt");
        StrategyGrammarLexer lexer = new StrategyGrammarLexer(charStream);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        StrategyGrammarParser parser = new StrategyGrammarParser(tokens);

        ru.pank.off.prodirect.antlr.StrategyGrammarBaseListenerImpl listener =
                new StrategyGrammarBaseListenerImpl(new VariableMap(Collections.emptyMap()));
        parser.addParseListener(listener);
        ParseTree tree = parser.prog();
        ParseTreeWalker walker = new ParseTreeWalker();
        walker.walk(listener, tree);
        assertEquals("Smoke parse test fail.", 366.25, listener.getResult());
    }

    @Test
    public void testParseSmoke_withEmbeddedVars() throws IOException {
        CharStream charStream = fromFileName("src/test/resources/parseWithEmbeddedVars.txt");
        StrategyGrammarLexer lexer = new StrategyGrammarLexer(charStream);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        StrategyGrammarParser parser = new StrategyGrammarParser(tokens);

        ru.pank.off.prodirect.antlr.StrategyGrammarBaseListenerImpl listener =
                new StrategyGrammarBaseListenerImpl(new VariableMap(
                        new HashMap<VariableMap.EmbeddedVar, Double>() {
                            {
                                put(VariableMap.EmbeddedVar.M1, 1d);
                                put(VariableMap.EmbeddedVar.SC1, 2d);
                            }
                        }
                ));
        parser.addParseListener(listener);
        ParseTree tree = parser.prog();
        ParseTreeWalker walker = new ParseTreeWalker();
        walker.walk(listener, tree);
        assertEquals("Smoke with vars parse test fail.", 366.25, listener.getResult());
    }

    @Test
    public void testJsonParsing() {


    }
}